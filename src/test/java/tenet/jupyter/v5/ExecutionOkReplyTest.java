package tenet.jupyter.v5;

import java.util.HashMap;
import java.util.Map;

import com.google.common.truth.Truth;
import org.testng.annotations.Test;

import static tenet.jupyter.JsonSubjectFactory.JSON;

public class ExecutionOkReplyTest {

    private ExecutionOkReply makeGeneric() {
        Map<String, String> userExpressions = new HashMap<>();
        userExpressions.put("first", "the first expression");
        userExpressions.put("second", "the second expression");
        return new ExecutionOkReply(33, userExpressions);
    }

    @Test
    public void testPagePayload() {
        Map<String, Object> bundle = new HashMap<>();
        bundle.put("text/plain", "some plain text");
        bundle.put("test/number", 55); // This seems wrong...

        ExecutionOkReply subj = makeGeneric();

        // Test
        subj.addPagePayload(bundle, 42);

        // Verify
        Truth.assertAbout(JSON).that(subj.payload)
             .matches("[ { 'data': { 'test/number': 55, 'text/plain': 'some plain text' }, "
                      + "'start': 42, 'source': 'page' } ]");
    }

    @Test
    public void testSetNextInputPayload() {
        ExecutionOkReply subj = makeGeneric();

        // Test
        subj.addSetNextInputPayload("the text", true);

        // Verify
        Truth.assertAbout(JSON).that(subj.payload)
             .matches("[ { 'text': 'the text', 'replace': true, 'source': 'set_next_input' } ]");
    }

    @Test
    public void testEditPayload() {
        ExecutionOkReply subj = makeGeneric();

        // Test
        subj.addEditPayload("the filename", 99);

        // Verify
        Truth.assertAbout(JSON).that(subj.payload)
             .matches("[ { 'filename': 'the filename', 'line_number': 99, 'source': 'edit' } ]");
    }

    @Test
    public void testAskExitPayload() {
        ExecutionOkReply subj = makeGeneric();

        // Test
        subj.addAskExitPayload(false);

        // Verify
        Truth.assertAbout(JSON).that(subj.payload)
             .matches("[ { 'keepkernel': false, 'source': 'ask_exit' } ]");
    }

}
