package tenet.jupyter.v5;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import tenet.jupyter.v5.AbstractKernel.Holder;

import static com.google.common.truth.Truth.assertThat;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static tenet.jupyter.v5.Sender.ExecutionState.busy;
import static tenet.jupyter.v5.Sender.ExecutionState.idle;

public class AbstractKernelTest {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractKernelTest.class);
    private static final VerificationMode once = times(1);
    @Mock Sender sender;
    @Mock ExecutorService service;
    @Mock Object monitor;
    @Mock MessageHeader header;
    MockKernel kernel;
    Map<String, String> args;

    boolean check; // Used by inner classes in some tests
    boolean check2;

    @BeforeMethod public void setup() {
        args = new HashMap<>();
        check = false;
        check2 = false;

        MockitoAnnotations.initMocks(this);

        // Now that initMocks is called, we can use these args.
        kernel = spy(new MockKernel(service, monitor, args));
    }

    @Test
    public void testConstructor() {
        assertThat(kernel.service).isNotNull();
        assertThat(kernel.userInputMonitor).isNotNull();
        assertThat(kernel.args).isNotNull();
        assertThat(kernel.executionCount.get()).isEqualTo(0);
    }

    @Test
    public void testDoStartupWhenServiceStarted() {
        when(service.isShutdown()).thenReturn(false);

        // Test
        kernel.doStartUp(sender, "engineId", singletonMap("test", "arg"));

        // Verify
        assertThat(kernel.sender).isSameAs(sender);
        assertThat(kernel.engineId).isEqualTo("engineId");
        assertThat(kernel.args).isEqualTo(singletonMap("test", "arg"));
        verify(service).isShutdown();
        verify(kernel, never()).makeThreadPool();
    }

    @Test
    public void testDoStartupWhenServiceNotStarted() {
        when(service.isShutdown()).thenReturn(true);

        // Test
        kernel.doStartUp(sender, "engineId", singletonMap("test", "arg"));

        // Verify
        verify(service).isShutdown();
        verify(kernel, once).makeThreadPool();
    }

    @Test(timeOut = 1000)
    public void testDoShutdown() throws InterruptedException {
        kernel.doShutdown(header);

        verify(service).shutdown();
        verify(service).awaitTermination(anyLong(), notNull());
    }

    @Test(timeOut = 1000)
    public void testDoShutdownInterrupted() throws InterruptedException {
        when(service.awaitTermination(anyLong(), any())).thenThrow(InterruptedException.class);

        kernel.doShutdown(header);

        verify(service).shutdown();
        verify(service).awaitTermination(anyLong(), notNull());
    }

    @Test
    public void testMakeThreadPool() {
        AbstractKernel subject = new AbstractKernel() {
            @Override protected ExecuteContext makeExecuteContext(ExecuteContext src) {
                return null;
            }

            @Override public KernelInfo getKernelInfo(MessageHeader hdr) {
                return null;
            }
        };

        ExecutorService service = subject.makeThreadPool();
        service.shutdown();
        assertThat(service).isNotNull();
    }

    @Test
    public void testDoExecuteWhenServiceShutdown() {
        kernel.executionCount.set(42);
        when(service.isShutdown()).thenReturn(true);

        // Test
        Reply actual = kernel.doExecute(header, "code", false, false, new HashMap<>(), false);

        // Verify
        assertThat(actual).isInstanceOf(ExecutionErrorReply.class);
        ExecutionErrorReply actualVal = (ExecutionErrorReply) actual;
        assertThat(actualVal.executionCount).isEqualTo(43);
        assertThat(actualVal.exceptionName).isEqualTo("NotStartedError");
        verify(sender, never()).postIoPubStatus(any(), any());
    }

    @Test
    public void testDoExecuteWhenMakeExecuteContextFails() {
        kernel.executionCount.set(42);
        kernel.makeExecuteContext = src -> {
            check = true;
            throw new UnknownError("kbaoom");
        };
        kernel.sender = sender;
        when(service.isShutdown()).thenReturn(false);

        // Test
        kernel.doExecute(header, "code", false, false, new HashMap<>(), false);

        // Verify
        verify(service).isShutdown();
        verify(sender, once).postIoPubStatus(header, busy);
        verify(sender, once).postIoPubExecuteInput(header, 43, "code");
        verify(sender, once).postShellExecution(eq(header), any(ExecutionErrorReply.class));
        verify(sender, once).postIoPubStatus(header, idle);
        assertThat(check).isTrue();
    }

    @Test
    public void testDoExecuteWhenPreexecuteFails() {
        kernel.executionCount.set(42);
        kernel.makeExecuteContext = src -> new ExecuteContext(src) {
            @Override public void preExecute() {
                check = true;
                throw new UnknownError("Fail please");
            }
            @Override public String execute() throws InterruptedException {
                throw new AssertionError("Shouldn't get here");
            }
        };
        kernel.sender = sender;
        when(service.isShutdown()).thenReturn(false);

        // Test
        try {
            kernel.doExecute(header, "code", false, false, new HashMap<>(), false);
        } finally {
            verify(service).isShutdown();
            verify(sender, once).postIoPubStatus(header, busy);
            verify(sender, once).postIoPubExecuteInput(header, 43, "code");
            verify(sender, once).postShellExecution(eq(header), any(ExecutionErrorReply.class));
            verify(sender, once).postIoPubStatus(header, idle);
            assertThat(check).isTrue();
            verify(service, never()).submit(any(Runnable.class));
        }
    }

    @Test
    public void testDoExecuteWhenExecuteFails() {
        kernel.executionCount.set(42);
        kernel.makeExecuteContext = src -> new ExecuteContext(src) {
            @Override public void preExecute() {
                assertThat(this.inWorkerThread).isFalse();
                check = true;
            }
            @Override public String execute() throws InterruptedException {
                assertThat(this.inWorkerThread).isTrue();
                check2 = true;
                throw new UnknownError("Fail please");
            }
        };
        kernel.sender = sender;
        when(service.isShutdown()).thenReturn(false);
        when(service.submit(any(Runnable.class))).then(invoc -> {
            invoc.<Runnable>getArgument(0).run();
            return null;
        });

        // Test
        kernel.doExecute(header, "code", false, false, new HashMap<>(), false);

        // Verify
        verify(service).isShutdown();
        verify(sender, once).postIoPubStatus(header, busy);
        verify(sender, once).postIoPubExecuteInput(header, 43, "code");
        verify(sender, once).postShellExecution(eq(header), any(ExecutionErrorReply.class));
        verify(sender, once).postIoPubStatus(header, idle);
        assertThat(check).isTrue();
        assertThat(check2).isTrue();
    }

    @Test
    public void testDoExecuteWhenExecuteSucceeds() {
        kernel.executionCount.set(42);
        kernel.makeExecuteContext = src -> new ExecuteContext(src) {
            @Override public void preExecute() {
                assertThat(this.inWorkerThread).isFalse();
                check = true;
            }
            @Override public String execute() throws InterruptedException {
                assertThat(this.inWorkerThread).isTrue();
                check2 = true;
                return "This did a thing!";
            }
        };
        kernel.sender = sender;
        when(service.isShutdown()).thenReturn(false);
        when(service.submit(any(Runnable.class))).then(invoc -> {
            invoc.<Runnable>getArgument(0).run();
            return null;
        });

        // Test
        kernel.doExecute(header, "code", false, false, new HashMap<>(), false);

        // Verify
        verify(service).isShutdown();
        verify(sender, once).postIoPubStatus(header, busy);
        verify(sender, once).postIoPubExecuteInput(header, 43, "code");
        verify(sender, once).postShellExecution(eq(header), any(ExecutionOkReply.class));
        verify(sender, once).postIoPubExecuteResult(header, 43,
                                                    singletonMap("text/plain", "This did a thing!"), emptyMap());
        verify(sender, once).postIoPubStatus(header, idle);
        assertThat(check).isTrue();
        assertThat(check2).isTrue();
    }

    private volatile String receivedInput;

    @Test(timeOut = 1000)
    public void testAcceptUserInputNormal() {
        receivedInput = null;
        ExecuteContext ctx = new Holder(kernel, sender, "engine", 22, header, "code", true, true,
                                        emptyMap(), true);
        ctx.inWorkerThread = true;
        new Thread(() -> {
            try {
                receivedInput = "got-in-thread";
                receivedInput = ctx.waitForStdinResponse();
            } catch (InterruptedException e) {
                LOG.warn("waiting interrupted", e);
            } catch (RuntimeException | Error e) {
                LOG.warn("other failure", e);
            }
        }).start();

        // Test
        kernel.acceptUserInput(header, "Incoming user input");

        // Verify
        assertThat(receivedInput).isEqualTo("Incoming user input");
    }

    @Test
    public void testHolderExecute() throws InterruptedException {
        Holder holder = new Holder(kernel, sender, "engineId", 22,
                                   header, "code", true, true, emptyMap(), true);

        // Test
        String actual = holder.execute();

        // Verify
        assertThat(actual).isNull();
    }

    @Test
    public void testHandleExecutionWhenReturnsNull() throws InterruptedException {
        ExecuteContext ctx
                = new Holder(kernel, sender, "engineId", 22,
                             header, "code", true, true, emptyMap(), true);

        // Test
        kernel.handleExecution(ctx);

        // Verify
        verify(sender, never()).postIoPubExecuteResult(any(), anyInt(), any(), any());
    }

    @Test
    public void testDoCompleteDefault() {
        Reply actual = kernel.doComplete(header, "code", 77);

        // Verify
        assertThat(actual).isInstanceOf(CodeCompletionOkReply.class);
    }

    @Test
    public void testDoIsCompleteDefault() {
        IsCompleteReply actual = kernel.doIsComplete(header, "code");

        // Verify
        assertThat(actual.status).isEqualTo(Reply.Status.unknown);
    }

    @Test
    public void testDoInspect() {
        Reply actual = kernel.doInspect(header, "code", 77, Kernel.DetailLevel.more);

        // Verify
        assertThat(actual).isInstanceOf(IntrospectionOkReply.class);
    }

    @Test
    public void testDoHistoryRange1() {
        Reply actual = kernel.doHistoryRange(header, false, false, 33, 22, 55);

        // Verify
        assertThat(actual).isInstanceOf(HistoryOkReply.class);
    }

    @Test
    public void testDoHistoryRange2() {
        Reply actual = kernel.doHistoryRange(header, true, false, 33, 22, 55);

        // Verify
        assertThat(actual).isInstanceOf(HistoryOkReply.class);
    }

    @Test
    public void testDoHistoryTail1() {
        Reply actual = kernel.doHistoryTail(header, false, false, 33);

        // Verify
        assertThat(actual).isInstanceOf(HistoryOkReply.class);
    }

    @Test
    public void testDoHistoryTail2() {
        Reply actual = kernel.doHistoryTail(header, true, false, 33);

        // Verify
        assertThat(actual).isInstanceOf(HistoryOkReply.class);
    }

    @Test
    public void testDoHistorySearch1() {
        Reply actual = kernel.doHistorySearch(header, false, false, ".*stuff.*", false);

        // Verify
        assertThat(actual).isInstanceOf(HistoryOkReply.class);
    }

    @Test
    public void testDoHistorySearch2() {
        Reply actual = kernel.doHistorySearch(header, true, false, ".*stuff.*", false);

        // Verify
        assertThat(actual).isInstanceOf(HistoryOkReply.class);
    }
}
