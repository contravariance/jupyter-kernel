package tenet.jupyter.v5;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;

public class MockKernel extends AbstractKernel {
    public MockKernel(ExecutorService svc, Object monitor, Map<String, String> args) {
        super(svc, monitor, args);
    }

    Function<ExecuteContext, ExecuteContext> makeExecuteContext;

    @Override public KernelInfo getKernelInfo(MessageHeader hdr) {
        LanguageInfo li = new LanguageInfo("name", "version", "mimetype", "fileExtension", "pygmentsLexer",
                                           "codemirrorMode", "nbconvertExporter");
        return KernelInfo.makeSimple("banner", "implementation", "impVersion", li,
                                     new KernelInfo.HelpLink("text1", "url1"),
                                     new KernelInfo.HelpLink("text2", "url2"));
    }

    @Override protected ExecuteContext makeExecuteContext(ExecuteContext src) {
        return makeExecuteContext.apply(src);
    }

    @Override protected ExecutorService makeThreadPool() {
        return service;
    }
}
