package tenet.jupyter.v5;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import tenet.jupyter.exceptions.KernelException;
import tenet.jupyter.v5.AbstractKernel.Holder;

import static com.google.common.truth.Truth.assertThat;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ExecuteContextTest {
    @Mock Sender sender;
    @Mock ExecutorService service;
    @Mock Object monitor;
    @Mock MessageHeader header;
    MockKernel kernel;
    Map<String, String> args;

    @BeforeMethod public void setup() {
        args = new HashMap<>();
        ExecuteContext.clock = Clock.fixed(Instant.ofEpochSecond(12345678L), ZoneId.of("-4"));

        MockitoAnnotations.initMocks(this);

        // Now that initMocks is called, we can use these args.
        kernel = spy(new MockKernel(service, monitor, args));
    }

    private ExecuteContext mockedGeneric() {
        return new Holder(kernel, sender, "engineId", 22, header, "code", true, true, emptyMap(), true);
    }

    @Test public void testDefaultPreexecute() {
        ExecuteContext subject = mockedGeneric();

        subject.preExecute();
    }

    @Test public void testGetCode() {
        ExecuteContext subject = mockedGeneric();

        assertThat(subject.getCode()).isEqualTo("code");
    }

    @Test public void testIsSilent() {
        ExecuteContext subject = mockedGeneric();

        assertThat(subject.isSilent()).isEqualTo(true);
    }

    @Test public void testIsStoreHistory() {
        ExecuteContext subject = mockedGeneric();

        assertThat(subject.isStoreHistory()).isEqualTo(true);
    }

    @Test public void testIsAllowStdin() {
        ExecuteContext subject = mockedGeneric();

        assertThat(subject.isAllowStdin()).isEqualTo(true);
    }

    @Test public void testGetStarted() {
        ExecuteContext subject = mockedGeneric();

        assertThat(subject.getStarted()).isEqualTo(Instant.ofEpochSecond(12345678L));
    }

    @Test public void testSetDependenciesMet() {
        ExecuteContext subject = mockedGeneric();

        assertThat(subject.getMetadata().get("dependencies_met")).isEqualTo(true);
        subject.setDependenciesMet(false);
        assertThat(subject.getMetadata().get("dependencies_met")).isEqualTo(false);
    }

    @Test public void testWriteStdout() {
        ExecuteContext subject = mockedGeneric();

        subject.writeStdout("some stdout");

        verify(sender).postIoPubStdout(header, "some stdout");
    }

    @Test public void testWriteStderr() {
        ExecuteContext subject = mockedGeneric();

        subject.writeStderr("some stderr");

        verify(sender).postIoPubStderr(header, "some stderr");
    }

    private ExecuteContext mockedForUserInput(boolean allowStdin) {
        return new ExecuteContext(kernel, sender, "engineId", 22, header, "code", true, true, emptyMap(), allowStdin) {
            @Override public String execute() throws InterruptedException {
                return "not used";
            }

            @Override String waitForStdinResponse() {
                return "user response";
            }
        };
    }

    @Test(timeOut = 1000) public void testPromptStdin() throws InterruptedException {
        ExecuteContext subject = mockedForUserInput(true);
        subject.inWorkerThread = true;

        // Test
        String actual = subject.promptStdin("test prompt");

        // Verify
        assertThat(actual).isEqualTo("user response");
        verify(sender).postStdinRequest(header, "test prompt");
    }

    @Test(timeOut = 1000) public void testPromptPassword() throws InterruptedException {
        ExecuteContext subject = mockedForUserInput(true);
        subject.inWorkerThread = true;

        // Test
        String actual = subject.promptStdinPassword("test prompt");

        // Verify
        assertThat(actual).isEqualTo("user response");
        verify(sender).postStdinPasswordRequest(header, "test prompt");
    }

    @Test(expectedExceptions = KernelException.class)
    public void testCheckStdinAllowedFail1() {
        ExecuteContext subject = mockedForUserInput(false);
        subject.inWorkerThread = true;

        subject.checkStdinAllowed();
    }

    @Test(expectedExceptions = KernelException.class)
    public void testCheckStdinAllowedFail2() {
        ExecuteContext subject = mockedForUserInput(true);
        subject.inWorkerThread = false;

        subject.checkStdinAllowed();
    }

    @Test
    public void testWriteResult1() {
        ExecuteContext subject = mockedGeneric();
        subject.writeResult("mimetype", "result");

        verify(sender).postIoPubExecuteResult(header, 22, singletonMap("mimetype", "result"), emptyMap());
    }

    @SuppressWarnings("unchecked") @Test
    public void testWriteResult2() {
        Map<String, String> data = new HashMap<>();

        ExecuteContext subject = mockedGeneric();
        subject.writeResult(data);

        verify(sender).postIoPubExecuteResult(header, 22, data, emptyMap());
    }

    @Test
    public void testSetExecuteStatusError1() {
        ExecuteContext subject = mockedGeneric();

        // Test
        assertThat(subject.getExecutionReply()).isInstanceOf(ExecutionOkReply.class);
        subject.setExecuteStatusError();

        // Verify
        Reply actual = subject.getExecutionReply();
        assertThat(actual).isInstanceOf(ExecutionErrorReply.class);

        // Check second call doesn't overwrite error.
        subject.setExecuteStatusError();
        assertThat(subject.getExecutionReply()).isSameAs(actual);
    }

    @Test
    public void testSetExecuteStatusError2() {
        ExecuteContext subject = mockedGeneric();
        List<String> traceback = Arrays.asList("trace1", "trace2");

        // Test
        subject.setExecuteStatusError("issue", "details", traceback);

        // Verify
        ExecutionErrorReply actual = (ExecutionErrorReply) subject.getExecutionReply();
        assertThat(actual.exceptionName).isEqualTo("issue");
        assertThat(actual.exceptionValue).isEqualTo("details");
        assertThat(actual.traceback).containsExactly("trace1", "trace2");
    }

    private ExecuteContext mockedForPostReply(boolean fail) {
        return new ExecuteContext(kernel, sender, "engineId", 22, header,
                                  "code", true, true, emptyMap(), false) {
            @Override public String execute() throws InterruptedException {
                return "not used";
            }

            @Override public void writeTracebackToConsole(ExecutionErrorReply reply) {
                checked = true;
                if (fail)
                    throw new UnknownError("Nuts");
            }
        };
    }

    boolean checked;

    @Test
    public void testPostExecuteReplyWhenReplyInvalidClass() {
        ExecuteContext subject = mockedForPostReply(false);
        checked = false;
        subject.executeReply = mock(Reply.class);

        // Test
        subject.postExecuteReply();

        // Verify
        assertThat(checked).isTrue();
        verify(sender, times(1)).postShellExecution(eq(header), any(ExecutionErrorReply.class));
    }

    @Test
    public void testPostExecuteReplyWhenWriteTracebackFails() {
        ExecuteContext subject = mockedForPostReply(true);
        checked = false;
        subject.executeReply = mock(Reply.class);

        // Test
        subject.postExecuteReply();

        // Verify
        assertThat(checked).isTrue();
        verify(sender, times(1)).postShellExecution(eq(header), any(ExecutionErrorReply.class));
    }
}
