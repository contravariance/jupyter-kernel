package tenet.jupyter;

import com.google.common.truth.FailureStrategy;
import com.google.common.truth.Subject;
import com.google.common.truth.SubjectFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;

/**
 * Subject factory to make assertions about objects that they match JSON.
 */
public class JsonSubjectFactory extends SubjectFactory<JsonSubjectFactory.JsonSubject, Object> {

    private static final Gson GSON = new GsonBuilder()
            .setFieldNamingStrategy(LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting()
            .create();
    public static final JsonSubjectFactory JSON = new JsonSubjectFactory();

    @Override public JsonSubject getSubject(FailureStrategy fs, Object that) {
        Object jsonTree;
        if (that instanceof JsonElement)
            jsonTree = that;
        else if (that instanceof String) {
            JsonParser parser = new JsonParser();
            jsonTree = parser.parse((String) that);
        } else {
            jsonTree = GSON.toJsonTree(that);
        }

        return new JsonSubject(fs, jsonTree);
    }

    public static class JsonSubject extends Subject<JsonSubject, Object> {
        public JsonSubject(FailureStrategy failureStrategy, Object jsonTree) {
            super(failureStrategy, jsonTree);
        }

        public void matches(String target) {
            JsonElement actual = (JsonElement) this.actual();
            JsonElement expected = new JsonParser().parse(target);
            if (!actual.equals(expected))
                this.fail("matches", GSON.toJson(expected));
        }

        @Override protected String actualCustomStringRepresentation() {
            return GSON.toJson((JsonElement) actual());
        }
    }
}
