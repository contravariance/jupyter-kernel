package tenet.jupyter.io;

import java.io.IOException;
import java.nio.CharBuffer;

import org.testng.annotations.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.nio.charset.CodingErrorAction.IGNORE;
import static java.nio.charset.StandardCharsets.UTF_8;

public class ByteBufferReaderTest {
    @Test public void defaultConstructor() {
        ByteBufferReader subj = new ByteBufferReader();

        assertThat(subj.charbuf.capacity()).isEqualTo(4096);
        assertThat(subj.bytebuf).isNull();
        assertThat(subj.decoder.charset().name()).isEqualTo("UTF-8");
        assertThat(subj.decoder.malformedInputAction()).isSameAs(IGNORE);
        assertThat(subj.decoder.unmappableCharacterAction()).isSameAs(IGNORE);
    }

    @Test public void readIntoCharArray() throws IOException {
        ByteBufferReader subj = new ByteBufferReader(7, UTF_8.newDecoder());
        String expected = "The quick brown fox jumps over the lazy dog.";
        byte[] source = expected.getBytes(UTF_8);
        subj.reassign(source);

        StringBuilder builder = new StringBuilder();
        char[] target = new char[5];
        while (true) {
            int count = subj.read(target, 0, 5);
            assertThat(subj.ready()).isTrue();
            if (count < 0)
                break;
            builder.append(target, 0, count);
        }
        assertThat(builder.toString()).isEqualTo(expected);
    }

    @Test public void readIntoCharBuffer() throws IOException {
        ByteBufferReader subj = new ByteBufferReader(7, UTF_8.newDecoder());
        String expected = "The_quick_brown_fox_jumps_over_the_lazy_dog.";
        byte[] source = expected.getBytes(UTF_8);
        subj.reassign(source);

        StringBuilder builder = new StringBuilder();
        CharBuffer target = CharBuffer.allocate(15);
        while (true) {
            int count = subj.read(target);
            assertThat(subj.ready()).isTrue();
            if (count < 0)
                break;
            if (count == 15)
                assertThat(target.hasRemaining()).isFalse();
            target.flip();
            builder.append(target);
        }
        assertThat(builder.toString()).isEqualTo(expected);
    }

    @Test public void readByteByByte() throws IOException {
        ByteBufferReader subj = new ByteBufferReader(7, UTF_8.newDecoder());
        String expected = "The_quick_brown_fox_jumps_over_the_lazy_dog.";
        byte[] source = expected.getBytes(UTF_8);
        subj.reassign(source);

        StringBuilder builder = new StringBuilder();
        while (true) {
            int oneChar = subj.read();
            if (oneChar < 0)
                break;
            builder.append((char) oneChar);
        }
        assertThat(builder.toString()).isEqualTo(expected);

        subj.close();
        assertThat(subj.bytebuf).isNull();
        assertThat(subj.charbuf.remaining()).isEqualTo(7);
    }

    @Test public void readIntoMixedTargets() throws IOException {
        ByteBufferReader subj = new ByteBufferReader(7, UTF_8.newDecoder());
        String expected = "The quick brown fox jumps over the lazy dog.";
        byte[] source = expected.getBytes(UTF_8);
        subj.reassign(source);

        StringBuilder builder = new StringBuilder();
        char[] array = new char[5];
        CharBuffer buffer = CharBuffer.allocate(6);

        while (true) {
            int count = subj.read(array, 0, 5);
            assertThat(subj.ready()).isTrue();
            if (count < 0)
                break;
            builder.append(array, 0, count);

            count = subj.read(buffer);
            assertThat(subj.ready()).isTrue();
            if (count < 0)
                break;
            buffer.flip();
            builder.append(buffer);

            int oneChar = subj.read();
            if (oneChar < 0)
                break;
            builder.append((char) oneChar);
        }
        assertThat(builder.toString()).isEqualTo(expected);
    }
}
