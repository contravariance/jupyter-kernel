package tenet.jupyter.io;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

import org.testng.annotations.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.nio.charset.CodingErrorAction.IGNORE;
import static java.nio.charset.StandardCharsets.UTF_8;

public class ByteBufferWriterTest {
    @Test public void defaultConstructor() {
        ByteBufferWriter subj = new ByteBufferWriter();

        assertThat(subj.charbuf.capacity()).isEqualTo(4096);
        assertThat(subj.bytebuf).isNull();
        assertThat(subj.encoder.charset().name()).isEqualTo("UTF-8");
        assertThat(subj.encoder.malformedInputAction()).isSameAs(IGNORE);
        assertThat(subj.encoder.unmappableCharacterAction()).isSameAs(IGNORE);
    }

    private static final String SAMPLE = "The_quick_brown_fox_jumps_over_the_lazy_dog.";

    @Test public void appendString() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        subj.reassign(new byte[128]);

        subj.append(SAMPLE);
        subj.close();

        assertThat(new String(subj.copyBytes())).isEqualTo(SAMPLE);
    }

    @Test public void writeStringCharByChar() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        subj.reassign(new byte[128]);

        for (int i = 0; i < SAMPLE.length(); i++) {
            subj.write(SAMPLE.charAt(i));
        }
        subj.close();

        assertThat(new String(subj.getBytes())).isEqualTo(SAMPLE);
    }

    @Test public void writeCharArray() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        subj.growable(2);

        subj.write(SAMPLE.toCharArray());
        subj.close();

        assertThat(new String(subj.getBytes())).isEqualTo(SAMPLE);
    }

    @Test public void appendStringCharByChar() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        subj.reassign(new byte[128]);

        for (int i = 0; i < SAMPLE.length(); i++) {
            subj.append(SAMPLE.charAt(i));
        }
        subj.close();

        assertThat(new String(subj.getBytes())).isEqualTo(SAMPLE);
    }

    @Test(expectedExceptions = BufferOverflowException.class)
     public void writeStringOverflow() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        subj.reassign(new byte[16]);

        subj.write(SAMPLE);
    }

    @Test public void getPaddedBytes() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        byte[] array = new byte[64];
        ByteBuffer buffer = ByteBuffer.wrap(array);
        subj.reassign(buffer);

        subj.write(SAMPLE);
        subj.close();

        assertThat(subj.getBuffer()).isSameAs(buffer);
        byte[] actual = subj.padBytes(32, '-');

        String expected = SAMPLE + "--------------------";
        assertThat(new String(actual, UTF_8)).isEqualTo(expected);
        assertThat(actual).isSameAs(array);
    }

    @Test public void getOriginalBytes() throws IOException {
        ByteBufferWriter subj = new ByteBufferWriter(7, UTF_8.newEncoder());
        byte[] array = "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef".getBytes(UTF_8);
        ByteBuffer buffer = ByteBuffer.wrap(array);
        subj.reassign(buffer);

        subj.write(SAMPLE);
        subj.close();

        assertThat(subj.getBuffer()).isSameAs(buffer);
        byte[] actual = subj.padBytes(32, -7);

        String expected = SAMPLE + "cdef0123456789abcdef";
        assertThat(new String(actual, UTF_8)).isEqualTo(expected);
        assertThat(actual).isSameAs(array);
    }
}
