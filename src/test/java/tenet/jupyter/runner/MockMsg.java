package tenet.jupyter.runner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;

class MockMsg {
    MockMsg(String... parts) {
        for (String part : parts) {
            this.parts.add(part.getBytes(UTF_8));
        }
    }
    final List<byte[]> parts = new ArrayList<>(8);

    List<String> getPartsStrings() {
        return parts.stream().map(part -> new String(part, UTF_8)).collect(Collectors.toList());
    }

    @Override public boolean equals(Object other) {
        if (!(other instanceof MockMsg)) {
            return false;
        }
        MockMsg that = (MockMsg) other;
        int size = this.parts.size();
        if (size != that.parts.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            boolean same = Arrays.equals(this.parts.get(i), that.parts.get(i));
            if (!same)
                return false;
        }
        return true;
    }

    private static final String DUMMY = "dummy_part";

    public List<byte[]> getParts() {
        return parts.isEmpty() ? singletonList(DUMMY.getBytes(UTF_8)) : parts;
    }
}
