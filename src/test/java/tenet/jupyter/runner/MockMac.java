package tenet.jupyter.runner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.Provider;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Mac;
import javax.crypto.MacSpi;

import static com.google.common.io.BaseEncoding.base16;
import static java.nio.charset.StandardCharsets.UTF_8;

public class MockMac extends Mac {
    public MockMac(ByteArrayOutputStream baos, byte[] digest) {
        super(new MockMacSpi(baos, digest), PROVIDER, "mock-algorithm");
    }

    public MockMac(ByteArrayOutputStream baos, String digest) {
        this(baos, base16().lowerCase().decode(digest));
    }

    private static final Provider PROVIDER = new MockProvider();

    private static class MockMacSpi extends MacSpi {
        MockMacSpi(ByteArrayOutputStream baos, byte[] digest) {
            this.baos = baos;
            this.digest = digest;
        }
        final ByteArrayOutputStream baos;
        final byte[] digest;

        @Override protected int engineGetMacLength() {
            return 4;
        }

        @Override protected void engineInit(Key key, AlgorithmParameterSpec algorithmParameterSpec)
        throws InvalidKeyException, InvalidAlgorithmParameterException {
            try {
                baos.write(key.getAlgorithm().getBytes(UTF_8));
                baos.write(':');
                baos.write(key.getEncoded());
                baos.write(';');
            } catch (IOException exc) {
                throw new UncheckedIOException(exc);
            }
        }

        @Override protected void engineUpdate(byte b) {
            baos.write(b);
        }

        @Override protected void engineUpdate(byte[] bytes, int off, int len) {
            baos.write(bytes, off, len);
        }

        @Override protected byte[] engineDoFinal() {
            return digest;
        }

        @Override protected void engineReset() {
            baos.write('*');
        }
    }

    private static class MockProvider extends Provider {
        /**
         * Constructs a provider with the specified name, version number,
         * and information.
         */
        protected MockProvider() {
            super("mock_provider", 1.1, "a moc provider");
        }
    }
}
