package tenet.jupyter.runner;

import javax.crypto.Mac;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;

import org.testng.annotations.Test;

import tenet.jupyter.runner.Msg.Header;

import static com.google.common.truth.Truth.assertThat;
import static java.util.Arrays.asList;
import static tenet.jupyter.TopTest.requote;

public class SendMessageTest {
    @Test void normalPath() throws InvalidKeyException {
        SendMessage msg = new SendMessage();
        msg.addIdentities(asList("identity-1".getBytes(), "identity-2".getBytes()));
        Header hdr = new Header();
        hdr.setMessageId("message id");
        hdr.setMessageType("test_message");
        hdr.setUsername("mockUser");
        hdr.setDate(Instant.ofEpochSecond(123456789L));
        hdr.setSession("mock kernel session");
        msg.setHeader(hdr);
        hdr.setMessageId("parent id");
        hdr.setMessageType("test_parent");
        hdr.setUsername("mockClient");
        hdr.setDate(Instant.ofEpochSecond(123459876L));
        hdr.setSession("mock client session");
        msg.setParentHeader(hdr);
        msg.setMetadata(Collections.singletonMap("meta", "data"));
        msg.setContent(Collections.singletonMap("status", "interesting"), Map.class);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Mac mockMac = new MockMac(baos, "71727374");
        mockMac.init(new SimpleKey("dummy key", "dummy algo"));
        Mocket socket = new Mocket();

        msg.sign(mockMac);
        msg.sendMultipart(socket, null);

        assertThat(socket.sent.size()).isEqualTo(1);
        MockMsg sentMsg = socket.sent.get(0);

        String hdr1=requote("{'msg_id':'message id','username':'mockUser','session':'mock kernel session',"
                                    + "'date':'1973-11-29T21:33:09Z','msg_type':'test_message','version':'5.0'}"),
                hdr2=requote("{'msg_id':'parent id','username':'mockClient','session':'mock client session',"
                                     + "'date':'1973-11-29T22:24:36Z','msg_type':'test_parent','version':'5.0'}");

        assertThat(sentMsg.getPartsStrings())
                .containsExactly("identity-1", "identity-2", "<IDS|MSG>", "71727374",
                                 hdr1, hdr2,
                                 "{\"meta\":\"data\"}                 ",
                                 "{\"status\":\"interesting\"}        ");
    }
}
