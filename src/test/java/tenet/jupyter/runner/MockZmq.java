package tenet.jupyter.runner;

class MockZmq implements ZMQAdapter {
    Channel channel;
    Mocket socket;
    boolean closed = false;
    MockZmq(Channel which, Mocket mocket) {
        this.channel = which;
        this.socket = mocket;
    }

    @Override public Channel pollInput() {
        return Channel.shell;
    }

    @Override public Socket forChannel(Channel channel) {
        if (channel != this.channel)
            throw new AssertionError("reading from wrong channel");
        return socket;
    }

    @Override public void close() {
        closed = true;
    }
}
