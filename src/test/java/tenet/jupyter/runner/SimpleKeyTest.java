package tenet.jupyter.runner;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.testng.annotations.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.nio.charset.StandardCharsets.UTF_8;

public class SimpleKeyTest {
    @Test void confirmConstruction() {
        SimpleKey subj = new SimpleKey("a test key", "algorithm");

        assertThat(subj.getAlgorithm()).isEqualTo("algorithm");
        assertThat(subj.getFormat()).isEqualTo("RAW");
        assertThat(new String(subj.getEncoded(), UTF_8)).isEqualTo("a test key");
        assertThat(subj.toString()).isEqualTo("SimpleKey(a test key, algorithm)");
    }

    private SimpleKey keyNum(int i) {
        return new SimpleKey("test_key_" + i, "algorithm" + (i / 8));
    }

    @Test void confirmStorableInSet() {
        int num = 64;
        Collection<Key> keys = new HashSet<>(num);
        for (int i = 0; i < num; i++) {
            keys.add(keyNum(i));
        }
        for (int i = 0; i < num; i++) {
            assertThat(keys).contains(keyNum(i));
        }
        keys = new ArrayList<>(keys);
        for (int i = 0; i < num; i++) {
            assertThat(keys).contains(keyNum(i));
        }
        assertThat(keys).doesNotContain("foobar");
    }
}
