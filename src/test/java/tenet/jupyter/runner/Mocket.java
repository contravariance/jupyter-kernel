package tenet.jupyter.runner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

class Mocket implements ZMQAdapter.Socket {
    Mocket(MockMsg... recvMsgs) {
        this(asList(recvMsgs));
    }

    Mocket(List<MockMsg> messages) {
        toRecv = messages;
    }

    final List<MockMsg> sent = new ArrayList<>(8);
    MockMsg sending = new MockMsg();
    final List<MockMsg> toRecv;
    int recvMsgIdx = -1;
    int recvMsgPartIdx = 0;

    @Override public void send(byte[] buffer, boolean sendMore) {
        sending.parts.add(buffer);
        if (!sendMore) {
            sent.add(sending);
            sending = new MockMsg();
        }
    }

    private MockMsg getMsg(int idx) {
        return idx >= toRecv.size() ? new MockMsg() : toRecv.get(idx);
    }

    @Override public byte[] recv() {
        if (recvMsgIdx < 0) {
            recvMsgIdx = 0;
        }
        while (true) {
            MockMsg msg = getMsg(recvMsgIdx);
            List<byte[]> parts = msg.getParts();
            if (recvMsgPartIdx >= parts.size()) {
                recvMsgIdx++;
                recvMsgPartIdx = 0;
                continue;
            }
            byte[] part = parts.get(recvMsgPartIdx);
            recvMsgPartIdx++;
            return part;
        }
    }

    /**
     * Unlike an iterator, hasMore will be false because no message has arrived. You have to recv
     * (after polling) to get the first message.
     * So this mock only claims there is more when it is a. pointing at a message and b. there is another part.
     * @return true if the current multi-part message has another part.
     */
    @Override public boolean hasReceiveMore() {
        if (recvMsgIdx < 0 || recvMsgIdx >= toRecv.size())
            return false;
        MockMsg msg = toRecv.get(recvMsgIdx);
        return recvMsgPartIdx < msg.parts.size();
    }
}
