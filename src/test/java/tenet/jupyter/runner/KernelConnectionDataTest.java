package tenet.jupyter.runner;

import java.io.Reader;
import java.io.StringReader;
import java.security.NoSuchAlgorithmException;

import org.testng.annotations.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.nio.charset.StandardCharsets.UTF_8;

public class KernelConnectionDataTest {
    @Test void fromFile() throws NoSuchAlgorithmException {
        Reader fileReader = new StringReader(CONNECTION_FILE);
        KernelConnectionData kcd = KernelConnectionData.fromFile(fileReader);
        assertThat(kcd.getKey().getAlgorithm()).isEqualTo("hmacsha256");
        assertThat(new String(kcd.getKey().getEncoded(), UTF_8))
                .isEqualTo("a0436f6c-1916-498b-8eb9-e81ab9368e84");
        assertThat(kcd.getChannelUrl(Channel.control)).isEqualTo("tcp://127.0.0.1:50160");
        assertThat(kcd.getChannelUrl(Channel.shell)).isEqualTo("tcp://127.0.0.1:57503");
        assertThat(kcd.getChannelUrl(Channel.stdin)).isEqualTo("tcp://127.0.0.1:52597");
        assertThat(kcd.getChannelUrl(Channel.hb)).isEqualTo("tcp://127.0.0.1:42540");
        assertThat(kcd.getChannelUrl(Channel.iopub)).isEqualTo("tcp://127.0.0.1:40885");
    }

    private static final String CONNECTION_FILE =
            "{\n  \"control_port\": 50160,\n  \"shell_port\": 57503,\n"
                    + "  \"transport\": \"tcp\",\n  \"signature_scheme\": \"hmac-sha256\",\n"
                    + "  \"stdin_port\": 52597,\n  \"hb_port\": 42540,\n  \"ip\": \"127.0.0.1\",\n"
                    + "  \"iopub_port\": 40885,\n  \"key\": \"a0436f6c-1916-498b-8eb9-e81ab9368e84\"\n}";
}
