package tenet.jupyter.runner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.testng.annotations.Test;

import static com.google.common.truth.Truth.assertThat;
import static tenet.jupyter.TopTest.requote;

public class RecvMessageTest {
    private List<String> convert(List<byte[]> listOfArrays) {
        return listOfArrays.stream().map(String::new).collect(Collectors.toList());
    }

    @Test(timeOut=1000) void normalPath() throws InvalidKeyException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MockMac mac = new MockMac(baos, "10111213");
        mac.init(new SimpleKey("mock key", "mock algo"));
        RecvMessage rec = new RecvMessage(mac);
        Mocket socket = new Mocket(new MockMsg("identity-1", "identity-2", "<IDS|MSG>",
                                               "10111213",
                                               "pHeader", "pParent", "pMetadata", "pContent",
                                               "buffer-1",
                                               "buffer-2"));
        // Test
        rec.accept(socket);
        rec.validate();
        assertThat(rec.isValid()).isTrue();

        // Verify
        assertThat(convert(rec.identities)).containsExactly("identity-1", "identity-2");
        assertThat(new String(rec.pHeader)).isEqualTo("pHeader");
        assertThat(new String(rec.pParent)).isEqualTo("pParent");
        assertThat(new String(rec.pMetadata)).isEqualTo("pMetadata");
        assertThat(new String(rec.pContent)).isEqualTo("pContent");
        // Disregarding buffers.
        assertThat(baos.toString()).isEqualTo("mock algo:mock key;pHeaderpParentpMetadatapContentbuffer-1buffer-2*");
    }

    @Test(timeOut=1000) void minimalPathWithJson() throws InvalidKeyException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MockMac mac = new MockMac(baos, "20212223");
        mac.init(new SimpleKey("mock key", "mock algo"));
        RecvMessage rec = new RecvMessage(mac);
        String hdr = requote("{'msg_id':'message id','session':'kernel','date':'2010-10-10T06:06:06Z',"
                                     + "'msg_type':'mock_request','version':'5.0'}"),
                phdr = requote("{'msg_id':'parent message','session':'remote','date':'2010-10-10T05:05:05Z',"
                                       + "'msg_type':'mock_reply','version':'5.0'}"),
                metadata = requote("{'some_key':'some_value','another_key':23}"),
                content = requote("{'code':'lol code','cursor_pos':23,'detail_level':1}");

        Mocket socket = new Mocket(new MockMsg("<IDS|MSG>", "20212223",
                                               hdr, phdr, metadata, content));

        // Test
        rec.accept(socket);
        rec.validate();
        assertThat(rec.isValid()).isTrue();

        // Verify
        assertThat(convert(rec.identities)).isEmpty();
        assertThat(rec.getHeader().getMessageId()).isEqualTo("message id");
        assertThat(rec.getParentHeader().getDate()).isEqualTo(Instant.parse("2010-10-10T05:05:05Z"));
        assertThat(rec.getMetadata().get("some_key")).isEqualTo("some_value");
        assertThat(rec.getContent().code).isEqualTo("lol code");
        // Disregarding buffers.
        String expected = "mock algo:mock key;" + hdr + phdr + metadata + content + "*";
        assertThat(baos.toString()).isEqualTo(expected);
    }

    @Test(timeOut=1000) void invalidated() throws InvalidKeyException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MockMac mac = new MockMac(baos, new byte[] {0x30, 0x31, 0x32, 0x33});
        mac.init(new SimpleKey("mock key", "mock algo"));
        RecvMessage rec = new RecvMessage(mac);
        Mocket socket = new Mocket(new MockMsg("<IDS|MSG>", "11223344",
                                               "pHeader", "pParent", "pMetadata", "pContent"));

        // Test
        rec.accept(socket);
        rec.validate();
        assertThat(rec.isValid()).isFalse();

        // Verify
        // Disregarding buffers.
        assertThat(baos.toString()).isEqualTo("mock algo:mock key;pHeaderpParentpMetadatapContent*");
    }

    @Test(timeOut=1000)
    void simulateReplay() throws InvalidKeyException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MockMac mac1 = new MockMac(baos, "40414243");
        mac1.init(new SimpleKey("mock key", "mock algo"));
        RecvMessage rec1 = new RecvMessage(mac1);
        MockMac mac2 = new MockMac(baos, "40414243");
        mac2.init(new SimpleKey("mock key", "mock algo"));
        RecvMessage rec2 = new RecvMessage(mac2);
        Mocket socket = new Mocket(new MockMsg("<IDS|MSG>", "40414243",
                                               "pHeader", "pParent", "pMetadata", "pContent"),
                                   new MockMsg("<IDS|MSG>", "40414243",
                                               "pHeader", "pParent", "pMetadata", "pContent"));

        // Test
        rec1.accept(socket);
        rec1.validate();
        assertThat(rec1.isValid()).isTrue();
        rec2.accept(socket);
        rec2.validate();
        assertThat(rec2.isValid()).isFalse();
    }

    @Test void testCulling() throws InvalidKeyException, IOException {
        int oldMaxSize = RecvMessage.MAX_SIZE;
        RecvMessage.MAX_SIZE = 5;
        try {
            List<MockMsg> msgs = new ArrayList<>(16);
            for (int num = 16; num < 32; num++) {
                msgs.add(new MockMsg("<IDS|MSG>", "505152" + Integer.toHexString(num),
                                     Integer.toString(num), "pParent", "pMetadata", "pContent"));
            }
            Mocket socket = new Mocket(msgs);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int num = 16; num < 32; num++) {
                MockMac mac = new MockMac(baos, "505152" + Integer.toHexString(num));
                mac.init(new SimpleKey("mock key", "mock algo"));
                RecvMessage rec = new RecvMessage(mac);
                rec.accept(socket);
                rec.validate();
                assertThat(rec.isValid()).isTrue();
                assertThat(Integer.parseInt(new String(rec.pHeader))).isEqualTo(num);
            }
        } finally {
            RecvMessage.MAX_SIZE = oldMaxSize;
        }
    }
}
