package tenet.jupyter;
import org.testng.annotations.Test;

/**
 * Make sure the test framework is run with -ea.
 */
public class TopTest {
    public static String requote(String jsonish) {
        return jsonish.replace('\'', '"');
    }

    @Test(expectedExceptions = {AssertionError.class})
    public void correctParams() {
        assert false : "Make sure that -ea was passed.";
    }
}
