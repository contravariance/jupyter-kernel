package tenet.jupyter;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.mockito.ArgumentMatcher;

/**
 * Match two json values.
 */
public class JsonStrMatch implements ArgumentMatcher<String> {
    public JsonStrMatch(String target) {
        this.target = target.replace('\'', '"');
    }

    private final String target;

    @Override public boolean matches(String argument) {
        JsonParser parser = new JsonParser();
        JsonElement targetJson = parser.parse(target);
        JsonElement argumentJson = parser.parse(argument);
        return targetJson.equals(argumentJson);
    }

    @Override public String toString() {
        return "json:" + target;
    }
}
