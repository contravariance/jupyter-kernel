package tenet.jupyter;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.mockito.ArgumentMatcher;

/**
 * Match two json values.
 */
public class JsonObjMatch implements ArgumentMatcher<Object> {
    public JsonObjMatch(String target) {
        this.target = target.replace('\'', '"');
    }

    private final String target;

    @Override public boolean matches(Object argument) {
        JsonParser parser = new JsonParser();
        JsonElement targetJson = parser.parse(target);
        JsonElement argumentJson = new Gson().toJsonTree(argument);
        return targetJson.equals(argumentJson);
    }

    @Override public String toString() {
        return "json:" + target;
    }
}
