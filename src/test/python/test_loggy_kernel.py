#!/usr/bin/env python3

import jupyter_kernel_test
import unittest

class LoggyKernelTests(jupyter_kernel_test.KernelTests):
    # Required --------------------------------------

    # The name identifying an installed kernel to run the tests against
    kernel_name = "loggy"

    # language_info.name in a kernel_info_reply should match this
    language_name = "tourettes-machine"

    # Optional --------------------------------------

    # Code in the kernel's language to write "hello, world" to stdout
    code_hello_world = "test_hello_world"

    # Pager: code that should display something (anything) in the pager
    code_page_something = "test_code_page"

    # Samples of code which generate a result value (ie, some text
    # displayed as Out[n])
    code_execute_result = [
        {'code': 'test_execute_result', 'result': 'dummy_result'}
    ]

    # Samples of code which should generate a rich display output, and
    # the expected MIME type
    code_display_data = [
        {'code': 'test_display_data', 'mime': 'image/png'}
    ]

if __name__ == '__main__':
    unittest.main()
