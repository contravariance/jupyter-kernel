/**
 * The runner package is the bulk of the java kernel implementation.
 *
 * <p>The {@link tenet.jupyter.runner.RunnerV5} class is intended to be compatible with v5 of the wire
 * protocol, much as the {@link tenet.jupyter.v5} package supports the v5 features. The upgrade plan, then
 * is to create a {@code V6} interface package and a {@code RunnerV6} implemenetation that is backwards
 * compatible with the V5 interface. (Unless, of course, the new features are similar enough that the
 * interface doesn't need to be updated.)
 */
package tenet.jupyter.runner;
