package tenet.jupyter.runner;

import org.zeromq.ZMQ;

/**
 * Identifies the different channels that a kernel needs to communicate on.
 */
enum Channel {
    hb(ZMQ.REP), control(ZMQ.ROUTER), shell(ZMQ.ROUTER), iopub(ZMQ.PUB), stdin(ZMQ.ROUTER);

    private final int socketType;
    private final String portField;

    Channel(int socketType) {
        this.socketType = socketType;
        this.portField = this.name() + "_port";
    }

    /**
     * Getter. This is a parameter used during socket creation.
     * @return the socket type number used when constructing the {@link ZMQ.Socket} object.
     */
    int getSocketType() {
        return socketType;
    }

    /**
     * Getter. This refers to a field within the JSON config file.
     * @return the name of the field that identifies the port that this channel's socket should connect to.
     */
    String getPortField() {
        return portField;
    }

    /**
     * Only the shell channel will reuse identities.
     * @return true if sending a message on this channel should send a copy of the parent message's identity.
     */
    public boolean transmitIdentity() {
        return this == shell || this == stdin;
    }
}
