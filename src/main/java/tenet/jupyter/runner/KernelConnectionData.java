package tenet.jupyter.runner;

import java.io.IOException;
import java.io.Reader;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.EnumMap;
import java.util.Map;
import java.util.UUID;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * The connection information required to run a kernel is passed in from the frontend as a JSON file.
 */
class KernelConnectionData {
    private final Map<Channel, Integer> ports = new EnumMap<>(Channel.class);
    private final Key actualKey;
    private final String key;
    private final String signatureScheme;
    private final String transport;
    private final String ip;

    private KernelConnectionData(String key, String signatureScheme,
                                 String transport, String ip) {
        this.actualKey = new SimpleKey(key, signatureScheme);
        this.key = key;
        this.signatureScheme = signatureScheme;
        this.transport = transport;
        this.ip = ip;
    }

    static KernelConnectionData fromFile(Reader input) throws NoSuchAlgorithmException {
        JsonObject top = new JsonParser().parse(input).getAsJsonObject();
        String transport = top.get("transport").getAsString();
        String ip = top.get("ip").getAsString();
        String signatureScheme = top.get("signature_scheme").getAsString();
        String key = top.get("key").getAsString();
        KernelConnectionData kcd = new KernelConnectionData(key, signatureScheme,
                                                            transport, ip);
        for(Channel channel : Channel.values()) {
            kcd.ports.put(channel, top.get(channel.getPortField()).getAsInt());
        }
        return kcd;
    }

    static KernelConnectionData makeDefault() {
        KernelConnectionData kcd = new KernelConnectionData(UUID.randomUUID().toString(), "hmac-sha256",
                                                            "tcp", "127.0.0.1");
        for(Channel channel : Channel.values()) {
            kcd.ports.put(channel, 0);
        }
        return kcd;
    }

    public int getPort(Channel channel) {
        return ports.get(channel);
    }

    public void setPort(Channel channel, int port) {
        ports.put(channel, port);
    }

    public String getChannelUrl(Channel channel) {
        int port = ports.get(channel);
        if (port == 0)
            return transport + "://" + ip;
        else
            return transport + "://" + ip + ':' + port;
    }

    public JsonObject asJson() throws IOException {
        JsonObject obj = new JsonObject();
        obj.addProperty("transport", transport);
        obj.addProperty("signature_scheme", signatureScheme);
        obj.addProperty("key", key);
        obj.addProperty("ip", ip);
        for (Channel chan : Channel.values()) {
            obj.addProperty(chan.getPortField(), ports.get(chan));
        }
        return obj;
    }

    Key getKey() {
        return actualKey;
    }
}
