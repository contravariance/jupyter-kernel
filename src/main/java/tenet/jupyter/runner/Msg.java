package tenet.jupyter.runner;

import java.time.Instant;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.v5.Message;
import tenet.jupyter.v5.MessageHeader;
import tenet.jupyter.v5.Sender.ExecutionState;

import static java.util.Collections.emptyMap;
import static tenet.jupyter.io.Stringify.bytesToHexString;

/**
 * Skeleton for serializing messages.
 */
class Msg {
    private static final Logger LOG = LoggerFactory.getLogger(Msg.class);
    /**
     * Handles most incoming messages without having to know the type beforehand.
     */
    static class Incoming {
        // execute_request
        String code;
        boolean silent;
        boolean storeHistory;
        Map<String, String> userExpressions;
        boolean allowStdin;

        // inspect_request
        // code
        int cursorPos;
        int detailLevel;

        // complete_request
        // code
        // cursorPos

        // history
        boolean output;
        boolean raw;
        String histAccessType;
        int session;
        int start;
        int stop;
        int n;
        String pattern;
        boolean unique;

        // is_complete_request
        // code

        // kernel_info_request
        // (nothing)

        // shutdown_request
        boolean restart;

        // input_reply
        String value;
    }

    /**
     * Corresponds to a standard message header. Also saves the identities for cases where we
     * need that for the response.
     */
    static class Header implements MessageHeader {
        private String msgId = "empty_msgId";
        private String username = "empty_username";
        private String session = "empty_session";
        private Instant date = null;
        private String msgType = "empty_message";
        private String version = "5.0";
        transient List<byte[]> identities = Collections.emptyList();

        @Override public String getMessageId() {
            return msgId;
        }
        @Override public String getUsername() {
            return username;
        }
        @Override public String getSession() {
            return session;
        }
        @Override public Instant getDate() {
            return date;
        }
        @Override public String getMessageType() {
            return msgType;
        }
        @Override public String getProtocolVersion() {
            return version;
        }
        void setMessageId(String msgId) {
            this.msgId = msgId;
        }
        void setUsername(String username) {
            this.username = username;
        }
        void setSession(String session) {
            this.session = session;
        }
        void setDate(Instant date) {
            this.date = date;
        }
        void setMessageType(String msgType) {
            this.msgType = msgType;
        }
        void setProtocolVersion(String version) {
            this.version = version;
        }

        @Override public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Header(msgId='").append(msgId)
              .append("', username='").append(username)
              .append("', session='").append(session)
              .append("', date='").append(date)
              .append("', msgType='").append(msgType)
              .append("', version='").append(version)
              .append("', identities=[");
            String comma = "";
            for (byte[] ident : identities) {
                sb.append(comma).append(bytesToHexString(ident));
                comma = ", ";
            }
            sb.append("])");
            return sb.toString();
        }

        public static MessageHeader dummy() {
            Header hdr = new Header();
            hdr.setMessageId("");
            hdr.setSession("no_session");
            hdr.setDate(Instant.now());
            hdr.setMessageType("no_message_type");
            hdr.setUsername("");
            return hdr;
        }
    }

    /**
     * Prompt a user for input on stdin.
     */
    static class Prompt {
        Prompt(String prompt, boolean password) {
            this.prompt = prompt;
            this.password = password;
        }
        String prompt;
        boolean password;
    }

    /**
     * Implements a shutdown reply.
     */
    static class Shutdown {
        private Shutdown(boolean restart) {
            this.restart = restart;
        }
        final boolean restart;
    }
    static final Shutdown SHUTDOWN = new Shutdown(false);
    static final Shutdown RESTART = new Shutdown(true);

    /**
     * Streaming data to post to stdout or stderr.
     */
    static class Stream {
        Stream(String name, String text) {
            this.name = name;
            this.text = text;
        }
        String name;
        String text;
    }

    static class DisplayData {
        DisplayData(Map<String, String> data, Map<String, Object> metadata) {
            this.data = data != null ? data : emptyMap();
            this.metadata = metadata != null ? metadata : emptyMap();
        }
        Map<String, String> data;
        Map<String, Object> metadata;
    }

    static class ExecutionStateMsg {
        final String executionState;

        private ExecutionStateMsg(String executionState) {
            this.executionState = executionState;
        }
    }
    static final Map<ExecutionState, ExecutionStateMsg> executionStateMsgs = new EnumMap<>(ExecutionState.class);
    static {
        for (ExecutionState state : ExecutionState.values()) {
            executionStateMsgs.put(state, new ExecutionStateMsg(state.toString()));
        }
    }

    /**
     * Broadcast message to indicate that execution is handling some input.
     */
    static class ExecuteInput extends Message {
        ExecuteInput(int executionCount, String code) {
            this.executionCount = executionCount;
            this.code = code;
        }

        int executionCount;
        String code;
    }

    /**
     * Broadcast message to indicate the results of an execution.
     */
    static class ExecuteResult extends Message {
        ExecuteResult(int executionCount, Map<String, String> data, Map<String, Object> metadata) {
            this.executionCount = executionCount;
            this.data = data;
            this.metadata = metadata;
        }
        int executionCount;
        Map<String, String> data;
        Map<String, Object> metadata;
    }
}
