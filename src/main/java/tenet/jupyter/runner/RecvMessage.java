package tenet.jupyter.runner;

import javax.crypto.Mac;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.exceptions.InvalidMessage;
import tenet.jupyter.runner.ZMQAdapter.Socket;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static tenet.jupyter.io.Stringify.bytesToHexString;
import static tenet.jupyter.io.Stringify.bytesToString;
import static tenet.jupyter.io.Stringify.ts;

/**
 * This is a message builder that focuses on accepting and authenticating multipart messages.
 */
class RecvMessage extends BaseMessage {
    private static final Logger LOG = LoggerFactory.getLogger(RecvMessage.class);
    static int MAX_SIZE = 1 << 16;
    private static final Set<SimpleKey> seenDigests = Collections.newSetFromMap(new ConcurrentHashMap<>(MAX_SIZE));

    private final Mac digestProvider;
    private boolean valid;

    RecvMessage(Mac digestProvider) {
        // This should already be initialized.
        this.digestProvider = digestProvider;
        this.valid = false;
    }

    private static byte[] recvLog(Socket socket, String what, boolean hexy) {
        byte[] part = socket.recv();
        if (LOG.isTraceEnabled()) {
            if (hexy)
                LOG.trace("RECV: {} {}", bytesToHexString(part), what);
            else
                LOG.trace("RECV: {} {}", bytesToString(part), what);
        }
        return part;
    }

    private byte[] digest(byte[] part) {
        digestProvider.update(part);
        return part;
    }

    private boolean finishSeen;
    private void assertHasMore(Socket socket) {
        if (!socket.hasReceiveMore()) {
            finishSeen = true;
            throw new InvalidMessage("Truncated message");
        }
    }

    boolean isValid() {
        return valid;
    }

    void accept(Socket socket) {
        this.valid = false;
        byte[] part = recvLog(socket, "identity-1", true);
        finishSeen = false;
        try {
            while (!Arrays.equals(DELIM, part)) {
                this.identities.add(part);
                assertHasMore(socket);
                part = recvLog(socket, "identity-n", true);
            }
            assertHasMore(socket);
            hmac = decodeHex(recvLog(socket, "hmac", false));
            assertHasMore(socket);
            pHeader = digest(recvLog(socket, "pHeader", false));
            assertHasMore(socket);
            pParent = digest(recvLog(socket, "pParent", false));
            assertHasMore(socket);
            pMetadata = digest(recvLog(socket, "pMetadata", false));
            assertHasMore(socket);
            pContent = digest(recvLog(socket, "pContent", false));
            while (socket.hasReceiveMore()) {
                digest(recvLog(socket, "buffer", false));
                // buffer.add(...);
            }
            finishSeen = true;
        } finally {
            // Flush the socket
            if (!finishSeen) {
                while (socket.hasReceiveMore()) {
                    socket.recv();
                }
            }
        }
    }

    private byte[] decodeHex(byte[] part) {
        return base16.decode(new String(part, US_ASCII));
    }

    /**
     * Validate the message hmac against the secret key.
     */
    void validate() {
        valid = false;
        // Otherwise validate the digest which we've been keeping up to date.
        byte[] digest = digestProvider.doFinal();
        valid = Arrays.equals(digest, hmac);
        LOG.trace("{} mac: digest={} msg={}",
                  valid ? "Valid" : "Invalid",
                  ts(()->base16.encode(digest)),
                  ts(()->base16.encode(hmac)));

        if (valid) {
            // Check for a replay attack per the reference implementation.
            if (!seenDigests.add(new SimpleKey(hmac))) {
                LOG.error("Seen this digest before!");
                valid = false;
            }
            if (seenDigests.size() > MAX_SIZE)
                cull();
        }
    }

    private static final Lock cullLock = new ReentrantLock();
    private static final Random cullRand = new SecureRandom();

    /**
     * This is basically what the reference implementation does, though I think you normally use an hmac with a nonce
     * and incrementing counter, but it's entirely possible I'm missing something in their implementation.
     */
    private static void cull() {
        if (!cullLock.tryLock())
            return; // Only one thread should be culling at a time.
        try {
            // As usual, check again once inside the mutex.
            int curSize = seenDigests.size();
            if (curSize > MAX_SIZE) {
                double numToCull = curSize / 10.0;
                // Borrowed this algo from https://eyalsch.wordpress.com/2010/04/01/random-sample/
                // and adapted for the Java iterator idiom.
                int visited = 0;

                Iterator<SimpleKey> iter = seenDigests.iterator();
                while (iter.hasNext()) {
                    iter.next();
                    if (cullRand.nextDouble() < numToCull / (curSize - visited)) {
                        iter.remove();
                        numToCull--;
                    }
                    visited++;
                }
            }
        } finally {
            cullLock.unlock();
        }
    }
}
