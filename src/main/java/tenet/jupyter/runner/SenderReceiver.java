package tenet.jupyter.runner;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import com.google.common.base.CaseFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.runner.Msg.ExecutionStateMsg;
import tenet.jupyter.runner.Msg.Header;
import tenet.jupyter.runner.Msg.Incoming;
import tenet.jupyter.runner.Msg.Prompt;
import tenet.jupyter.runner.Msg.Shutdown;
import tenet.jupyter.runner.Msg.Stream;
import tenet.jupyter.v5.CodeCompletionOkReply;
import tenet.jupyter.v5.ErrorReply;
import tenet.jupyter.v5.ExecutionErrorReply;
import tenet.jupyter.v5.ExecutionOkReply;
import tenet.jupyter.v5.HistoryOkReply;
import tenet.jupyter.v5.IntrospectionOkReply;
import tenet.jupyter.v5.IsCompleteReply;
import tenet.jupyter.v5.Kernel;
import tenet.jupyter.v5.Kernel.DetailLevel;
import tenet.jupyter.v5.KernelInfo;
import tenet.jupyter.v5.Message;
import tenet.jupyter.v5.MessageHeader;
import tenet.jupyter.v5.Reply;
import tenet.jupyter.v5.Sender;

import static tenet.jupyter.runner.KernelSignal.sigContinue;
import static tenet.jupyter.runner.KernelSignal.sigRestart;
import static tenet.jupyter.runner.KernelSignal.sigShutdown;
import static tenet.jupyter.runner.SenderReceiver.ReplyMsgType.*;
/**
 * The base of the runner has to receive messages from the kernel server.
 */
class SenderReceiver implements Sender {
    private static final Logger LOG = LoggerFactory.getLogger(SenderReceiver.class);
    private static final DetailLevel[] LEVELS = DetailLevel.values();
    private final Kernel kernel;
    private final Engine engine;

    SenderReceiver(Kernel kernel, Engine engine) {
        this.kernel = kernel;
        this.engine = engine;
    }

    /**
     * Same as handle, but includes the content and copy arguments that are in the Python server.
     * @param channel the channel the message arrived on
     * @param msg the multipart message
     */
    final KernelSignal handle(Channel channel, RecvMessage msg) {
        if (msg == null) {
            LOG.error("Null message on {}", channel);
            return sigContinue;
        }
        Header hdr = msg.getHeader();
        hdr.identities = msg.getIdentities();
        LOG.debug("Recv {} on {}", hdr.getMessageType(), channel);
        Kernel kernel = this.kernel;
        Incoming content = msg.getContent();
        switch(channel) {
        case shell:
        case control: // Same as shell, higher priority
            switch(hdr.getMessageType()) {
            case "execute_request":
                Reply execAnswer =
                        kernel.doExecute(hdr, content.code, content.silent, content.storeHistory,
                                         content.userExpressions, content.allowStdin);
                this.postShellExecution(hdr, execAnswer);
                break;
            case "inspect_request":
                Reply introAnswer =
                        kernel.doInspect(hdr, content.code, content.cursorPos, LEVELS[content.detailLevel]);
                this.postShellIntrospection(hdr, introAnswer);
                break;
            case "complete_request":
                Reply ccAnswer =
                        kernel.doComplete(hdr, content.code, content.cursorPos);
                this.postShellCodeCompleted(hdr, ccAnswer);
                break;
            case "history_request":
                Reply histAnswer;
                switch(content.histAccessType) {
                case "range":
                    histAnswer = kernel.doHistoryRange(hdr, content.output, content.raw, content.session,
                                                       content.start, content.stop);
                    break;
                case "tail":
                    histAnswer = kernel.doHistoryTail(hdr, content.output, content.raw, content.n);
                    break;
                case "search":
                    histAnswer = kernel.doHistorySearch(hdr, content.output, content.raw,
                                                        content.pattern, content.unique);
                    break;
                default:
                    histAnswer = ErrorReply.makeSimple("Unrecognized history_access_type",
                                                       content.histAccessType);
                }
                this.postShellHistory(hdr, histAnswer);
                break;
            case "is_complete_request":
                IsCompleteReply isCompAnswer = kernel.doIsComplete(hdr, content.code);
                this.postShellIsComplete(hdr, isCompAnswer);
                break;
            case "kernel_info_request":
                LOG.trace("KIR -- check with kernel");
                KernelInfo kiAnswer = kernel.getKernelInfo(hdr);
                LOG.trace("KIR -- ready to send");
                this.postShellKernelInfo(hdr, kiAnswer);
                LOG.trace("KIR -- response away");
                break;
            case "shutdown_request":
                this.postShellShutdownAck(hdr, content.restart);
                kernel.doShutdown(hdr);
                return content.restart ? sigRestart : sigShutdown;
            default:
                LOG.error("Unexpected shell message type: {}", hdr.getMessageType());
            }
            break;
        case stdin:
            switch(hdr.getMessageType()) {
            case "input_reply":
                kernel.acceptUserInput(hdr, content.value);
                break;
            default:
                LOG.error("Unexpected stdin message type: {}", hdr.getMessageType());
            }
            break;
        default:
            LOG.error("Didn't expect channel {} to get {}", channel, hdr.getMessageType());
        }
        return sigContinue;
    }

    private void warnUnexpectedReply(MessageHeader token, Reply value) {
        if (value != null) {
            LOG.error("Issue in {}: {} wasn't an OkReply or ErrorReply", token,
                      value.getClass().getSimpleName());
        }
    }

    private void postShellCodeCompleted(MessageHeader token, Reply reply) {
        post2(completeReply, token, reply, CodeCompletionOkReply.class, ErrorReply.class);
    }
    @Override public void postShellCodeCompleted(MessageHeader token, CodeCompletionOkReply reply) {
        post(completeReply, token, reply, CodeCompletionOkReply.class);
    }
    @Override public void postShellCodeCompleted(MessageHeader token, ErrorReply reply) {
        post(completeReply, token, reply, ErrorReply.class);
    }

    private void postShellExecution(MessageHeader token, Reply reply) {
        post2(executeReply, token, reply, ExecutionOkReply.class, ExecutionErrorReply.class);
    }
    @Override public void postShellExecution(MessageHeader token, ExecutionOkReply reply) {
        post(executeReply, token, reply, ExecutionOkReply.class);
    }
    @Override public void postShellExecution(MessageHeader token, ExecutionErrorReply reply) {
        post(executeReply, token, reply, ExecutionErrorReply.class);
    }

    private void postShellHistory(MessageHeader token, Reply reply) {
        post2(historyReply, token, reply, HistoryOkReply.class, ErrorReply.class);
    }
    @Override public void postShellHistory(MessageHeader token, HistoryOkReply reply) {
        post(historyReply, token, reply, HistoryOkReply.class);
    }
    @Override public void postShellHistory(MessageHeader token, ErrorReply reply) {
        post(historyReply, token, reply, ErrorReply.class);
    }

    private void postShellIntrospection(MessageHeader token, Reply reply) {
        post2(inspectReply, token, reply, IntrospectionOkReply.class, ErrorReply.class);
    }
    @Override public void postShellIntrospection(MessageHeader token, IntrospectionOkReply reply) {
        post(inspectReply, token, reply, IntrospectionOkReply.class);
    }
    @Override public void postShellIntrospection(MessageHeader token, ErrorReply reply) {
        post(inspectReply, token, reply, ErrorReply.class);
    }

    @Override public void postShellIsComplete(MessageHeader token, IsCompleteReply reply) {
        post(isCompleteReply, token, reply, IsCompleteReply.class);
    }

    @Override public void postShellKernelInfo(MessageHeader token, KernelInfo reply) {
        post(kernelInfoReply, token, reply, KernelInfo.class);
    }

    @Override public void postIoPubStdout(MessageHeader token, String data) {
        post(stream, token, new Stream("stdout", data), Stream.class);
    }

    @Override public void postIoPubStderr(MessageHeader token, String data) {
        post(stream, token, new Stream("stderr", data), Stream.class);
    }

    @Override public void postIoPubStatus(MessageHeader token, ExecutionState state) {
        post(status, token, Msg.executionStateMsgs.get(state), ExecutionStateMsg.class);
    }

    @Override public void postIoPubExecuteInput(MessageHeader token, int executionCount, String code) {
        post(executeInput, token, new Msg.ExecuteInput(executionCount, code), Msg.ExecuteInput.class);
    }

    @Override public void postIoPubExecuteResult(MessageHeader token, int executionCount, Map<String, String> data,
                                                 Map<String, Object> metadata) {
        post(executeResult, token, new Msg.ExecuteResult(executionCount, data, metadata), Msg.ExecuteResult.class);
    }

    @Override public void postIoPubDisplayData(MessageHeader token, Map<String, String> data,
                                                 Map<String, Object> metadata) {
        post(displayData, token, new Msg.DisplayData(data, metadata), Msg.DisplayData.class);
    }

    @Override public MessageHeader postStdinRequest(MessageHeader token, String prompt) {
        return post(inputRequest, token, new Prompt(prompt, false), Prompt.class).getHeader();
    }

    @Override public MessageHeader postStdinPasswordRequest(MessageHeader token, String prompt) {
        return post(inputRequest, token, new Prompt(prompt, true), Prompt.class).getHeader();
    }

    private void postShellShutdownAck(MessageHeader token, boolean restart) {
        post(shutdownReply, token, restart ? Msg.RESTART : Msg.SHUTDOWN, Shutdown.class);
    }

    /**
     * Most replies have either an Ok form or an Error form.
     * @param msgType the message type for the reply
     * @param parent the parent header
     * @param reply the reply object
     * @param okType the OK class
     * @param errorType the Error class
     */
    private void post2(ReplyMsgType msgType, MessageHeader parent, Reply reply,
                       Class<?> okType, Class<?> errorType) {
        if(okType.isInstance(reply)) {
            post(msgType, parent, reply, okType);
        } else if(errorType.isInstance(reply)) {
            post(msgType, parent, reply, errorType);
        } else {
            warnUnexpectedReply(parent, reply);
        }
    }

    private SendMessage post(ReplyMsgType msgType, MessageHeader parent, Object obj, Type type) {
        SendMessage send = new SendMessage();
        Header hdr = new Header();
        if (msgType.channel.transmitIdentity() && parent instanceof Header) {
            send.addIdentities(((Header) parent).identities);
        }
        LOG.debug("Send {} on {}", msgType.label, msgType.channel);
        send.setParentHeader(parent);
        hdr.setSession(engine.getEngineId());
        hdr.setUsername(engine.getUsername());
        hdr.setMessageId(UUID.randomUUID().toString());
        hdr.setDate(Instant.now());
        hdr.setMessageType(msgType.label);
        send.setHeader(hdr);
        if (obj instanceof Message) {
            send.setMetadata(((Message) obj).metadataPart);
        }
        send.setContent(obj, type);
        engine.toSocket(msgType.channel, send);
        return send;
    }

    enum ReplyMsgType {
        executeReply,
        completeReply,
        historyReply,
        inspectReply,
        isCompleteReply,
        connectReply,
        commInfoReply,
        kernelInfoReply,
        shutdownReply,
        inputRequest(Channel.stdin),
        stream(Channel.iopub),
        displayData(Channel.iopub),
        executeInput(Channel.iopub),
        executeResult(Channel.iopub),
        error(Channel.iopub),
        status(Channel.iopub);

        private final Channel channel;
        private final String label;

        ReplyMsgType() {
            this(Channel.shell);
        }

        ReplyMsgType(Channel channel) {
            this.channel = channel;
            this.label = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, this.name());
        }
    }
}
