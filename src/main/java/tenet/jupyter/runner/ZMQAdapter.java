package tenet.jupyter.runner;

/**
 * None of the ZMQ classes implement an interface; this wraps the functionality we use.
 */
interface ZMQAdapter extends AutoCloseable {
    Channel pollInput();
    Socket forChannel(Channel channel);
    @Override void close();

    interface Socket {
        void send(byte[] buffer, boolean sendMore);
        byte[] recv();
        boolean hasReceiveMore();
    }
}
