package tenet.jupyter.runner;

import javax.crypto.Mac;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.runner.ZMQAdapter.Socket;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static tenet.jupyter.io.Stringify.bytesToHexString;
import static tenet.jupyter.io.Stringify.bytesToString;

/**
 * Similar to RecvMessage, only we have more control over the messages we're sending.
 */
class SendMessage extends BaseMessage {
    private static final Logger LOG = LoggerFactory.getLogger(SendMessage.class);

    SendMessage() { }

    void addIdentities(Collection<byte[]> identities) {
        this.identities.addAll(identities);
    }

    private static void sendLog(Socket socket, byte[] part, boolean sendMore, String what, boolean hexy) {
        socket.send(part, sendMore);
        if (LOG.isTraceEnabled()) {
            if (hexy)
                LOG.trace("SEND: {} {}", bytesToHexString(part), what);
            else
                LOG.trace("SEND: {} {}", bytesToString(part), what);
        }
    }

    void sign(Mac mac) {
        mac.update(pHeader);
        mac.update(pParent);
        mac.update(pMetadata);
        mac.update(pContent);
        // buffers.forEach(mac::update);
        byte[] hmacBytes = mac.doFinal();
        hmac = base16.encode(hmacBytes).getBytes(US_ASCII);
    }

    public void sendMultipart(Socket socket, Channel channel) {
        // Get copies to ensure consistent synchronized access.
        byte[] sendHmac = hmac,
                sendHeader = pHeader,
                sendParent = pParent,
                sendMeta = pMetadata,
                sendContent = pContent;
        // Ensure that two threads don't try to send simultaneously.
        synchronized (socket) {
            LOG.trace("-------- SEND: {} ----------", channel);
            identities.forEach(part -> sendLog(socket, part, true, "identity", true));
            sendLog(socket, DELIM, true, "delim", false);
            sendLog(socket, sendHmac, true, "hmac", false);
            sendLog(socket, sendHeader, true, "header", false);
            sendLog(socket, sendParent, true, "parent", false);
            sendLog(socket, sendMeta, true, "metadataPart", false);
            sendLog(socket, sendContent, false, "content", false);
        }
        // buffers.forEach(sender);
    }
}
