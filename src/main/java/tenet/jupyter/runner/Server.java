package tenet.jupyter.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static tenet.jupyter.io.Stringify.bytesLazy;

/**
 * Provides a poller to handle the 0MQ channels, and a thread that will handle incoming events.
 */
class Server implements Runnable {
    // private final ZMQ.Poller poll;
    private final SenderReceiver senderReceiver;
    private final Engine engine;
    private final ZMQAdapter zmq;
    private static final Logger LOG = LoggerFactory.getLogger(Server.class);

    private Server(SenderReceiver senderReceiver, Engine engine, ZMQAdapter zmq) {
        this.senderReceiver = senderReceiver;
        this.engine = engine;
        this.zmq = zmq;
    }

    @Override public void run() {
        Thread current = Thread.currentThread();
        KernelSignal signal = KernelSignal.sigContinue;
        LOG.debug("Begin run");

        while(!current.isInterrupted() && signal == KernelSignal.sigContinue) {
            try {
                Channel channel = zmq.pollInput();
                if (channel == Channel.hb) {
                    LOG.trace("Echoing heartbeat");
                    ZMQAdapter.Socket socket = zmq.forChannel(channel);
                    do {
                        byte[] data = socket.recv();
                        LOG.trace("Heartbeat packet: {}", bytesLazy(data));
                        socket.send(data, socket.hasReceiveMore()); // echo heartbeat
                    } while(socket.hasReceiveMore());
                } else {
                    RecvMessage msg = engine.fromSocket(channel);
                    if (!msg.isValid()) {
                        LOG.debug("Channel={} rejecting invalid message: {}", channel, msg);
                        continue;
                    }
                    LOG.trace("Accepting message on channel {}", channel);
                    signal = senderReceiver.handle(channel, msg);
                    LOG.trace("Handling complete");
                }
            } catch (Error | RuntimeException exc) {
                LOG.error("Problem handling message", exc);
            }
        }
        LOG.debug("Run complete");
    }

    /**
     * Constructs a kernel server.
     * @param senderReceiver a receiver to handle incoming messages
     * @param zmq the zmq connections
     * @return a connected server
     */
    static Server make(SenderReceiver senderReceiver,
                       Engine engine,
                       ZMQAdapter zmq) {

        return new Server(senderReceiver, engine, zmq);
    }
}
