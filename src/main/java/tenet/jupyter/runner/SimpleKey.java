package tenet.jupyter.runner;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;
import java.util.regex.Pattern;

import tenet.jupyter.io.Stringify;

/**
 * We initialize the session with a key. Same as {@link SecretKeySpec}, only this removes
 * things we don't care about.
 */
final class SimpleKey implements SecretKey {
    private static final Pattern DROP_DASH = Pattern.compile("-", Pattern.LITERAL);
    private final byte[] key;
    private final String algorithm;

    SimpleKey(String key, String algorithm) {
        this(Stringify.stringToBytes(key), algorithm);
    }

    SimpleKey(byte[] key) {
        this(key, "dummy");
    }

    private SimpleKey(byte[] key, String algorithm) {
        this.key = key;
        this.algorithm = DROP_DASH.matcher(algorithm).replaceAll("");
    }

    @Override public String getAlgorithm() {
        return algorithm;
    }

    @Override public String getFormat() {
        return "RAW";
    }

    @Override public byte[] getEncoded() {
        return Arrays.copyOf(key, key.length);
    }

    @Override public int hashCode() {
        return Arrays.hashCode(key) * 31 + algorithm.hashCode();
    }

    @Override public boolean equals(Object other) {
        return other instanceof SimpleKey
                && Arrays.equals(key, ((SimpleKey) other).key)
                && algorithm.equals(((SimpleKey) other).algorithm);
    }

    @Override public String toString() {
        return "SimpleKey(" + Stringify.bytesToString(key)+ ", " + algorithm + ')';
    }
}
