package tenet.jupyter.runner;

import org.zeromq.ZMQ;

import static java.util.Objects.requireNonNull;
import static org.zeromq.ZMQ.Poller.POLLIN;

/**
 * Creates the sockets and registers them with a poller.
 */
class ZMQImpl implements ZMQAdapter {
    ZMQImpl(KernelConnectionData kcd) {
        poller = new ZMQ.Poller(channels.length);
        sockets = new SockWrapper[channels.length];
        ZMQ.Context ctx = ZMQ.context(1);
        for(Channel chan : channels) {
            int sockType = chan.getSocketType();
            boolean isListen = sockType == ZMQ.REP || sockType == ZMQ.ROUTER || sockType == ZMQ.PUB;
            ZMQ.Socket sock = ctx.socket(chan.getSocketType());
            sockets[chan.ordinal()] = new SockWrapper(sock);
            // Set to 1s to prevent hangs at exit.
            sock.setLinger(1000L);
            if (isListen) {
                String address = kcd.getChannelUrl(chan);
                if (kcd.getPort(chan) == 0) {
                    int port = sock.bindToRandomPort(address);
                    kcd.setPort(chan, port);
                } else {
                    sock.bind(address);
                }
            } else {
                sock.connect(kcd.getChannelUrl(chan));
            }
            if(chan.getSocketType() == ZMQ.SUB) {
                sock.subscribe(DUMMY_TOPIC);
            }
            int num = poller.register(sock, POLLIN);
            if (num != chan.ordinal())
                throw new AssertionError("Counting doesn't seem to work.");
        }
    }

    private final ZMQ.Poller poller;
    private static final Channel[] channels = Channel.values();
    private final SockWrapper[] sockets;
    private static final byte[] DUMMY_TOPIC = new byte[0];

    @Override public Channel pollInput() {
        poller.poll();
        for (int idx = 0; idx < channels.length; idx++) {
            if (poller.pollin(idx)) {
                return channels[idx];
            }
        }
        throw new IllegalStateException();
    }

    @Override public Socket forChannel(Channel channel) {
        return sockets[channel.ordinal()];
    }

    @Override public void close() {
        for (SockWrapper sock : sockets) {
            if (sock != null) {
                sock.socket.close();
                poller.unregister(sock.socket);
            }
        }
    }

    private static class SockWrapper implements ZMQAdapter.Socket {
        final ZMQ.Socket socket;

        private SockWrapper(ZMQ.Socket socket) {
            this.socket = requireNonNull(socket);
        }

        @Override public void send(byte[] buffer, boolean sendMore) {
            this.socket.send(buffer, sendMore ? ZMQ.SNDMORE : 0);
        }

        @Override public byte[] recv() {
            return this.socket.recv();
        }

        @Override public boolean hasReceiveMore() {
            return this.socket.hasReceiveMore();
        }
    }
}
