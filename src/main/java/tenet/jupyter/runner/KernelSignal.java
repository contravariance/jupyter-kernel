package tenet.jupyter.runner;

/**
 * Kernel signals from messages. Combines signals to avoid throwing away a shutdown message.
 */
enum KernelSignal {
    sigContinue, sigRestart, sigShutdown
}
