package tenet.jupyter.runner;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.exceptions.InvalidMessage;
import tenet.jupyter.io.ByteBufferReader;
import tenet.jupyter.io.ByteBufferWriter;
import tenet.jupyter.io.InstantAdapter;
import tenet.jupyter.runner.Msg.Header;
import tenet.jupyter.runner.Msg.Incoming;
import tenet.jupyter.v5.MessageHeader;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static tenet.jupyter.io.Stringify.bytesToHexString;
import static tenet.jupyter.io.Stringify.bytesToString;

/**
 * Most of the storage is the same, but sending and receiving require
 * different state. And to keep things simple, we don't try to cache any parsing, on the odd
 * chance we have to, it's not really that expensive.
 */
abstract class BaseMessage {
    static final byte[] DELIM = "<IDS|MSG>".getBytes(US_ASCII);
    private static final byte[] EMPTY = "{}".getBytes(US_ASCII);
    private static final Logger LOG = LoggerFactory.getLogger(BaseMessage.class);
    final List<byte[]> identities = new ArrayList<>(1);
    byte[] hmac;
    byte[] pHeader = EMPTY;
    byte[] pParent = EMPTY;
    byte[] pMetadata = EMPTY;
    byte[] pContent = EMPTY;
    // final List<byte[]> buffers = new ArrayList<>(0);
    final static BaseEncoding base16 = BaseEncoding.base16().lowerCase();

    List<byte[]> getIdentities() {
        return identities;
    }

    Header getHeader() {
        return parse(pHeader, Header.class);
    }
    void setHeader(MessageHeader value) {
        pHeader = dump(value, Header.class);
    }

    Header getParentHeader() {
        return parse(pParent, Header.class);
    }
    void setParentHeader(MessageHeader parent) {
        pParent = dump(parent, Header.class);
    }

    private static final Type METADATA_TYPE = new TypeToken<Map<String, Object>>() {}.getType();
    void setMetadata(Map<String, Object> value) {
        pMetadata = dump(value, METADATA_TYPE);
    }
    Map<String, Object> getMetadata() {
        return parse(pMetadata, METADATA_TYPE);
    }

    Incoming getContent() {
        return parse(pContent, Incoming.class);
    }
    void setContent(Object value, Type type) {
        pContent = dump(value, type);
    }

    private <T> T parse(byte[] part, Type type) {
        // requireNonNull(part);
        try {
            ByteBufferReader in = reader.get().reassign(part != null ? part : EMPTY);
            // LOG.debug("Obtained reader...");
            return gson.fromJson(in, type);
        } catch (JsonParseException exc) {
            throw new InvalidMessage(bytesToString(part), exc);
        }
    }

    private byte[] dump(Object obj, Type type) {
        ByteBufferWriter out = writer.get().growable(32);
        // LOG.debug("Writing out {} with type {}", obj, type.getTypeName());
        gson.toJson(obj, type, new JsonWriter(out));
        // LOG.debug("Gson says: {}", ts(() -> gson.toJson(obj, type)));
        // We'll always allocate a new buffer, so hang on to the array unless there's a ton of slack.
        return out.padBytes(32, ' ');
    }

    private static final Gson gson;
    static {
        gson = new GsonBuilder().disableHtmlEscaping()
                                .serializeNulls()
                                .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES)
                                .registerTypeAdapter(Instant.class, new InstantAdapter())
                                .create();
    }

    private final static ThreadLocal<ByteBufferReader> reader = ThreadLocal.withInitial(ByteBufferReader::new);

    private final static ThreadLocal<ByteBufferWriter> writer = ThreadLocal.withInitial(ByteBufferWriter::new);

    @Override public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName())
          .append("{")
          .append("identities=[");
        String comma = "";
        for (byte[] ident : identities) {
            sb.append(comma).append(bytesToHexString(ident));
            comma = ", ";
        }
        sb.append("], hmac=").append(bytesToHexString(hmac))
          .append(", pHeader=").append(bytesToString(pHeader))
          .append(", pParent=").append(bytesToString(pParent))
          .append(", pMetadata=").append(bytesToString(pMetadata))
          .append(", pContent=").append(bytesToString(pContent))
          .append('}');
        return sb.toString();
    }
}
