package tenet.jupyter.runner;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.v5.Kernel;
import tenet.jupyter.v5.Sender;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.newBufferedReader;
import static java.util.Objects.requireNonNull;
import static tenet.jupyter.runner.KernelConnectionData.fromFile;
import static tenet.jupyter.runner.KernelConnectionData.makeDefault;

/**
 * This is the main entry point for a kernel.
 */
public class RunnerV5 {
    private static final Logger LOG = LoggerFactory.getLogger(RunnerV5.class);
    public static void main(String[] args) throws Exception {
        Map<String, String> params = new HashMap<>(args.length >> 1);
        for (int index = 0; index < args.length; index += 2) {
            if (args[index].startsWith("--")) {
                params.put(args[index].substring(2), args[index + 1]);
            } else {
                throw new IllegalArgumentException("All arguments must begin with '--'.");
            }
        }

        String redirect = params.get("redirect-stderr");
        if (redirect != null) {
            System.setErr(new PrintStream(new FileOutputStream(redirect, true)));
        }
        String filePath = params.get("connection-file");
        String className = requireNonNull(params.get("kernel-class"));
        LOG.info("Loading kernel named {}.", className);
        Kernel kernel = (Kernel) Class.forName(className).newInstance();
        KernelConnectionData kcd;
        if (filePath != null) {
            LOG.info("Reading connection file at {}.", filePath);
            kcd = fromFile(newBufferedReader(Paths.get(filePath), UTF_8));
        } else {
            LOG.info("No connection file specified; constructing default connection object.");
            kcd = makeDefault();
        }

        LOG.info("Set up ZMQ.");
        try (ZMQAdapter zmqAdapter = new ZMQImpl(kcd)) {
            if (filePath == null) {
                LOG.warn("Run jupyter notebook --existing /abs/path/yourfile.json with:\n{}",
                         kcd.asJson());
            }
            LOG.info("Set up auth engine.");
            Engine engine = new Engine(kcd.getKey(), zmqAdapter,
                                       params.get("engine-id"),
                                       params.get("username"));
            LOG.info("Engine has id {}.", engine.getEngineId());
            LOG.info("Set up receiver.");
            SenderReceiver senderReceiver = new SenderReceiver(kernel, engine);
            LOG.info("Construct server.");
            Server server = Server.make(senderReceiver, engine, zmqAdapter);
            LOG.info("Indicate server is starting.");
            senderReceiver.postIoPubStatus(Msg.Header.dummy(), Sender.ExecutionState.starting);
            LOG.info("Notify kernel of startup.");
            kernel.doStartUp(senderReceiver, engine.getEngineId(), params);
            LOG.info("Indicate server is ready.");
            senderReceiver.postIoPubStatus(Msg.Header.dummy(), Sender.ExecutionState.idle);
            LOG.info("Kernel server run.");
            server.run();
        } finally {
            LOG.info("Exiting.");
        }
    }
}
