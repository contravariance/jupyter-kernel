package tenet.jupyter.runner;

import javax.crypto.Mac;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.exceptions.InvalidConfiguration;

/**
 * Contains the global state for the engine.
 */
class Engine {
    private static final Logger LOG = LoggerFactory.getLogger(Engine.class);
    private final String engineId;
    private final String username;
    private final Key key;
    private final ThreadLocal<Mac> stashMac = ThreadLocal.withInitial(this::newMac);
    private final ZMQAdapter zmq;

    Engine(Key key, ZMQAdapter zmq,  String engineId, String username) {
        this.key = key;
        this.zmq = zmq;
        this.engineId = engineId != null ? engineId : UUID.randomUUID().toString();
        this.username = username != null ? username : "kernel";
    }

    private Mac newMac() {
        try {
            Mac mac = Mac.getInstance(key.getAlgorithm());
            mac.init(key);
            return mac;
        } catch (NoSuchAlgorithmException | InvalidKeyException exc) {
            throw new InvalidConfiguration(exc);
        }
    }

    /**
     * Gets the engineId, really a "session" for all messages (Replies) from the server.
     * @return this is used as the session for all messages coming from this kernel.
     */
    String getEngineId() {
        return engineId;
    }

    /**
     * Gets the username, which is basically a dummy name.
     * @return this is used as the username for all messages coming from this kernel.
     */
    String getUsername() {
        return username;
    }

    RecvMessage fromSocket(Channel channel) {
        ZMQAdapter.Socket socket = zmq.forChannel(channel);
        Mac mac = stashMac.get();
        try {
            RecvMessage msg = new RecvMessage(mac);
            msg.accept(socket);
            msg.validate();
            return msg;
        } finally {
            mac.reset();
        }
    }

    void toSocket(Channel channel, SendMessage msg) {
        Mac mac = stashMac.get();
        try {
            msg.sign(mac);
        } finally {
            mac.reset();
        }
        msg.sendMultipart(zmq.forChannel(channel), channel);
    }
}
