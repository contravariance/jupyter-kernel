package tenet.jupyter.io;

import java.io.Writer;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.function.IntFunction;

import static java.nio.charset.CodingErrorAction.IGNORE;

/**
 * A reassignable writer that avoids layers of adapters. Each byte array you're assigning to costs one ByteBuffer.wrap
 * operation.
 *
 * <p>This also has a growable option: if the encoder reports an overflow, it will attempt to allocate a new buffer
 * and continue encoding. A user specified buffer may be provided with a lambda to control allocation. And,
 * of course, buffers may be reused.
 */
public class ByteBufferWriter extends Writer {
    ByteBuffer bytebuf;
    private IntFunction<ByteBuffer> allocateBuffer;
    final CharBuffer charbuf;
    final CharsetEncoder encoder;

    /**
     * Construct a writer with a charbuffer capacity and a UTF-8 encoder that ignores problems.
     */
    public ByteBufferWriter() {
        this(4096, StandardCharsets.UTF_8.newEncoder()
                                         .onMalformedInput(IGNORE)
                                         .onUnmappableCharacter(IGNORE));
    }

    /**
     * Construct a writer with a charbuffer capacity and a custom encoder.
     * @param capacity the buffering capacity
     * @param encoder the preferred charset
     */
    public ByteBufferWriter(int capacity, CharsetEncoder encoder) {
        this.charbuf = CharBuffer.allocate(capacity);
        // [POS] empty space ... [LIM/CAP]
        this.encoder = encoder;
    }

    /**
     * Reset the reader to accept data from a {@link ByteBuffer} byte array.
     * @param array a byte array
     * @return this instance for fluency.
     */
    public ByteBufferWriter reassign(byte[] array) {
        reassign(array, 0, array.length);
        return this;
    }

    /**
     * Reset the reader to accept data from a {@link ByteBuffer}.
     * @param buffer a source bytebuffer.
     * @return this instance for fluency.
     */
    public ByteBufferWriter reassign(ByteBuffer buffer) {
        growable(buffer, null);
        return this;
    }

    /**
     * Reset the reader to accept data from a {@link ByteBuffer} byte array.
     * @param array a byte array
     * @param offset start reading at this point
     * @param len read this much
     * @return this instance for fluency.
     */
    public ByteBufferWriter reassign(byte[] array, int offset, int len) {
        reassign(ByteBuffer.wrap(array, offset, len));
        return this;
    }

    /**
     * Assign an initial {@link ByteBuffer} that will be discarded and reallocated
     * if the content is too large for it.
     * @param initialCap the initial capacity of the byte buffer
     * @return this instance for fluency.
     */
    public ByteBufferWriter growable(int initialCap) {
        ByteBuffer initial = ByteBuffer.allocate(initialCap);
        growable(initial, capacity -> ByteBuffer.allocate(Math.max(capacity * 2, 4)));
        return this;
    }

    /**
     * Assign an initial {@link ByteBuffer} that will be discarded and reallocated
     * if the content is too large for it.
     * @param initial a bytebuffer that is ready to accept content.
     * @param growFunc a function to allocate a buffer larger than the original
     * @return this instance for fluency.
     */
    public ByteBufferWriter growable(ByteBuffer initial, IntFunction<ByteBuffer> growFunc) {
        bytebuf = initial;
        charbuf.clear();
        encoder.reset();
        this.allocateBuffer = growFunc;
        return this;
    }

    /**
     * Get the underlying bytebuffer.
     *
     * <p>This will not be the original if reassigned by {@link #growable}.
     * @return the underlying bytebuffer.
     */
    public ByteBuffer getBuffer() {
        return bytebuf;
    }

    @Override public void write(int c) {
        charbuf.put((char) c);
        lazyFlush(false);
    }

    @Override public void write(String str, int off, int len) {
        append(str, off, off + len);
    }

    @Override public Writer append(CharSequence csq) {
        append(csq, 0, csq.length());
        return this;
    }

    @Override public Writer append(CharSequence csq, int start, int end) {
        int idx = start;
        while (idx < end) {
            int stop = Math.min(end, idx + charbuf.remaining());
            charbuf.append(csq, idx, stop);
            idx = stop;
            lazyFlush(idx < end);
        }
        return this;
    }

    @Override public Writer append(char c) {
        charbuf.put(c);
        lazyFlush(false);
        return this;
    }

    @Override public void write(char[] cbuf, int off, int len) {
        int idx = off, end = off + len;
        while (idx < end) {
            int amount = Math.min(end - idx, charbuf.remaining());
            charbuf.put(cbuf, idx, amount);
            idx += amount;
            lazyFlush(idx < end);
        }
    }

    private void lazyFlush(boolean force) {
        if(force || !charbuf.hasRemaining()) {
            // charbuf.hasRemaining:
            //       data ready to flush [POS] slack [LIM/CAP]
            // !charbuf.hasRemaining:
            //       data ready to flush [POS/LIM/CAP]
            charbuf.flip();
            // [POS] data ready to flush [LIM] ... [CAP]
            CoderResult result;
            do {
                result = encoder.encode(charbuf, bytebuf, false);
                if(result.isOverflow()) {
                    if(allocateBuffer == null) {
                        throw new BufferOverflowException();
                    } else {
                        ByteBuffer makeMyMonsterGrow = allocateBuffer.apply(bytebuf.capacity());
                        // bytebuf: data need to write out [POS] slack [LIM/CAP]
                        bytebuf.flip();
                        // [POS]    data need to write out [LIM] slack [CAP]
                        makeMyMonsterGrow.put(bytebuf);
                        bytebuf = makeMyMonsterGrow;
                    }
                }
            } while(!result.isUnderflow());
            charbuf.clear();
        }
    }

    @Override public void flush() {
        lazyFlush(true);
    }

    @Override public void close() {
        flush();
    }

    /**
     * Flush and return a copy of the bytes written out.
     * @param allowSlack tolerate this many bytes trailing the data rather than copy. &lt;0 will always copy.
     * @param pad a padding value, &lt;0 to not pad.
     * @return the bytes written out.
     */
    public byte[] padBytes(int allowSlack, int pad) {
        lazyFlush(true);
        int limit = bytebuf.limit(), cap = bytebuf.capacity(), pos = bytebuf.position();
        // data written in buffer [POS] slack space [LIM/CAP]
        if (allowSlack >=0
                && bytebuf.hasArray()
                && cap - pos <= allowSlack) {
            byte[] result = bytebuf.array();
            if (pad > 0)
                Arrays.fill(result, pos, cap, (byte) pad);
            return result;
        } else {
            //       data written in buffer [POS] slack space [LIM/CAP]
            byte[] result = new byte[pos];
            bytebuf.flip();
            // [POS] data written in buffer [LIM] slack space [CAP]
            bytebuf.get(result);
            // Now set them back so it's writable again. This first to keep limit > position.
            bytebuf.limit(limit);
            // [POS] data written in buffer       slack space [LIM/CAP]
            bytebuf.position(pos);
            //       data written in buffer [POS] slack space [LIM/CAP]
            return result;
        }
    }

    public byte[] copyBytes() {
        return padBytes(-1, -1); // Force a copy.
    }

    public byte[] getBytes() {
        lazyFlush(true);

        return padBytes(-1, (byte) 0);
    }
}
