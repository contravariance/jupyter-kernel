package tenet.jupyter.io;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;

/**
 * A GSON type adapter for dates.
 */
public class InstantAdapter extends TypeAdapter<Instant> {
    @Override public void write(JsonWriter out, Instant value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.value(ISO_INSTANT.format(value));
        }
    }

    @Override public Instant read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }
        String string = in.nextString();
        try {
            return ISO_INSTANT.parse(string, Instant::from);
        } catch(DateTimeParseException exc) {
            TemporalAccessor ta = ISO_DATE_TIME.parseBest(string, ZonedDateTime::from, LocalDateTime::from);
            if (ta instanceof ZonedDateTime) {
                return ((ZonedDateTime) ta).toInstant();
            } else {
                return ((LocalDateTime) ta).toInstant(UTC);
            }

        }
    }
}
