package tenet.jupyter.io;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;

import static java.nio.charset.CodingErrorAction.IGNORE;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * A reassignable reader that avoids layers of adapters. Each byte array you're reading then costs one ByteBuffer.wrap
 * operation.
 */
public class ByteBufferReader extends Reader {
    ByteBuffer bytebuf;
    final CharBuffer charbuf;
    final CharsetDecoder decoder;

    public ByteBufferReader(int capacity, CharsetDecoder decoder) {
        this.decoder = decoder;
        charbuf = CharBuffer.allocate(capacity);
    }

    /**
     * Construct a reader with typical defaults.
     */
    public ByteBufferReader() {
        this(4096, UTF_8.newDecoder().onMalformedInput(IGNORE)
                .onUnmappableCharacter(IGNORE));
    }

    /**
     * Reset the reader to accept data from a {@link ByteBuffer} byte array.
     * @param array a byte array
     * @return this instance, for fluency
     */
    public ByteBufferReader reassign(byte[] array) {
        reassign(array, 0, array.length);
        return this;
    }

    /**
     * Reset the reader to accept data from a {@link ByteBuffer}. You can reassign the same buffer
     * if you just want to reset everything.
     * @param buffer a source bytebuffer.
     * @return this instance, for fluency
     */
    public ByteBufferReader reassign(ByteBuffer buffer) {
        bytebuf = buffer;
        charbuf.clear().flip(); // Nothing for us to read yet.
        decoder.reset();
        return this;
    }

    /**
     * Reset the reader to accept data from a {@link ByteBuffer} byte array.
     * @param array a byte array
     * @param offset start reading at this point
     * @param len read this much
     * @return this instance, for fluency
     */
    public ByteBufferReader reassign(byte[] array, int offset, int len) {
        reassign(ByteBuffer.wrap(array, offset, len));
        return this;
    }

    /**
     * The charbuffer is always in read mode.
     * If there's no data to read, flip to write mode using compact,
     * add any available data, and flip back to read mode.
     */
    private void decode() {
        // Only decode if there's nothing left to read.
        if (!charbuf.hasRemaining()) {
            // Compact changes to write mode.
            charbuf.clear(); // pos = 0, limit = cap
            // If there's anything to write, do so.
            if (bytebuf.hasRemaining()) {
                decoder.decode(bytebuf, charbuf, true);
            }
            charbuf.flip(); // After a write operation, always flip back to reading.
        }
    }

    @Override public int read(CharBuffer target) {
        int count = 0;
        if (charbuf.hasRemaining()) {
            try {
                count = charbuf.read(target);
            } catch(IOException e) {
                throw new UncheckedIOException(e);
            }
            charbuf.flip();
            charbuf.compact();
            // moves unread to start, ready to write to again.
        }
        if (target.hasRemaining()) {
            // After charbuf is exhausted, read directly into the target.
            int pos = target.position();
            decoder.decode(bytebuf, target, true);
            count += (target.position() - pos);
        }
        return determineEof(count);
    }

    @Override public int read() throws IOException {
        decode();
        return charbuf.hasRemaining() ? charbuf.get() : -1;
    }

    @Override public boolean ready() throws IOException {
        return true;
    }

    @Override public int read(char[] cbuf, int off, int len) throws IOException {
        decode();
        int amount = Math.min(len, charbuf.remaining());
        charbuf.get(cbuf, off, amount);
        return determineEof(amount);
    }

    private int determineEof(int amount) {
        if (amount > 0)
            return amount;
        return bytebuf.hasRemaining() || charbuf.hasRemaining() ? 0 : -1;
    }

    @Override public void close() {
        bytebuf = null;
        charbuf.clear();
        decoder.reset();
    }
}
