package tenet.jupyter.io;

import java.util.function.Supplier;

import com.google.common.io.BaseEncoding;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Utility class to do some conversions between binary and IO.
 *
 * <p>The class itself can be used to create a lazy toString method.
 */
public final class Stringify {
    private final Supplier<String> stringSupplier;

    private Stringify(Supplier<String> expr) {
        this.stringSupplier = expr;
    }

    /**
     * Null safe conversion of a byte array to a string.
     * @param array an array containing UTF-8 encoded text
     * @return the decoded string
     */
    public static String bytesToString(byte[] array) {
        if (array == null) {
            return "null";
        } else {
            return new String(array, UTF_8);
        }
    }

    /**
     * Null safe conversion of a string to a byte array.
     * @param string some string
     * @return the encoded array
     */
    public static byte[] stringToBytes(String string) {
        if (string == null) {
            string = "null";
        }
        return string.getBytes(UTF_8);
    }

    /**
     * Conversion of a byte array to a lowercase base 16 encoded string.
     * @param array an array of bytes.
     * @return A string containing
     */
    public static String bytesToHexString(byte[] array) {
        return BaseEncoding.base16().lowerCase().encode(array);
    }

    /**
     * A lazy object that decodes a string.
     * @param array an array of bytes.
     * @return an object whose toString will perform the conversion on demand.
     */
    public static Object bytesLazy(byte[] array) {
        return ts(() -> bytesToString(array));
    }

    @Override public String toString() {
        return stringSupplier.get();
    }

    /**
     * Factory method to construct a lazy toString instance.
     * @param supplier a lambda that supplies a string.
     * @return A lazy toString instance.
     */
    public static Object ts(Supplier<String> supplier) {
        return new Stringify(supplier);
    }
}
