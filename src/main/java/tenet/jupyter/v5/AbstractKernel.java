package tenet.jupyter.v5;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;
import static tenet.jupyter.v5.Sender.ExecutionState.busy;
import static tenet.jupyter.v5.Sender.ExecutionState.idle;

/**
 * A basic kernel implementation. This has one worker thread to handle code execution and
 * allow for handling stdin and such.
 *
 * @see MultithreadedKernel Uses a multi-threaded executor service.
 */
public abstract class AbstractKernel implements Kernel {
    public AbstractKernel() {
        this(null, new Object(), new HashMap<>());
    }

    AbstractKernel(ExecutorService service, Object userInputMonitor, Map<String, String> args) {
        executionCount = new AtomicInteger(0);
        this.service = service == null ? makeThreadPool() : service;
        this.userInputMonitor = userInputMonitor;
        this.args = args;
    }

    protected Sender sender;
    protected String engineId;
    protected final Map<String, String> args;
    protected final AtomicInteger executionCount;
    volatile ExecutorService service;
    final Object userInputMonitor;
    String userInput;
    private final static Logger LOG = LoggerFactory.getLogger(AbstractKernel.class);

    /**
     * A subclass must provide an implementation of ExecuteContext.
     * @param src the source data to provide to the super() constructor.
     * @return a concrete ExecuteContext instance
     */
    protected abstract ExecuteContext makeExecuteContext(ExecuteContext src);

    @Override public void doStartUp(Sender sender, String engineId, Map<String, String> args) {
        LOG.warn("doStartup: {}, {}, {}", sender, engineId, args);
        if (service.isShutdown())
            service = makeThreadPool();
        this.sender = sender;
        this.engineId = engineId;
        this.args.clear();
        this.args.putAll(args);
    }

    @Override public void doShutdown(MessageHeader hdr) {
        ExecutorService serviceLocal = service;
        serviceLocal.shutdown();
        try {
            serviceLocal.awaitTermination(15, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            // proceed
        }
    }

    /**
     * Creates the thread pool. Override this to enable multiple code execution threads.
     * @return an ExecutorService
     * @see MultithreadedKernel a straightforward override
     */
    protected ExecutorService makeThreadPool() {
        return Executors.newSingleThreadExecutor();
    }

    @Override
    public Reply doExecute(MessageHeader hdr, String code, boolean silent, boolean storeHistory,
                           Map<String, String> userExpressions, boolean allowStdin)
    {
        int executionCount = this.executionCount.incrementAndGet();
        ExecutorService serviceLocal = service;
        if (serviceLocal.isShutdown()) {
            return new ExecutionErrorReply(executionCount, "NotStartedError",
                                           "Execution requested while not started",
                                           emptyList());
        }
        LOG.debug("status: busy");
        sender.postIoPubStatus(hdr, busy);
        final ExecuteContext context;
        try {
            sender.postIoPubExecuteInput(hdr, executionCount, code);
            LOG.debug("status: create temp context");
            ExecuteContext holder =
                    new Holder(this, sender, engineId,
                               executionCount, hdr, code, silent,
                               storeHistory, userExpressions, allowStdin);
            LOG.debug("status: create execute context");
            context = makeExecuteContext(holder);
        } catch(RuntimeException | Error any) {
            ExecutionErrorReply err = ExecutionErrorReply.makeSimple(executionCount, any);
            sender.postShellExecution(hdr, err);
            sender.postIoPubStatus(hdr, idle);
            return null;
        }
        try {
            context.preExecute();
            serviceLocal.submit(() -> {
                try {
                    context.inWorkerThread = true;
                    this.handleExecution(context);
                } catch (RuntimeException | InterruptedException | Error exc) {
                    LOG.debug("execute: error", exc);
                    context.setExecuteStatusError(exc);
                } finally {
                    context.postExecuteReply();
                    sender.postIoPubStatus(hdr, idle);
                }
            });
        } catch(RuntimeException | Error any) {
            context.setExecuteStatusError(any);
            context.postExecuteReply();
            sender.postIoPubStatus(hdr, idle);
        }

        return null;
    }

    @Override public Reply doComplete(MessageHeader hdr, String code, int pos) {
        return CodeCompletionOkReply.makeNocompletion(pos);
    }

    @Override public IsCompleteReply doIsComplete(MessageHeader hdr, String code) {
        return IsCompleteReply.unknown();
    }

    @Override public Reply doInspect(MessageHeader hdr, String code, int cursorPos, DetailLevel detailLevel) {
        return IntrospectionOkReply.makeNotFound();
    }

    @Override
    public Reply doHistoryRange(MessageHeader hdr, boolean output, boolean raw, int session, int start, int stop) {
        return output ? HistoryOkReply.makeInOutHistory(emptyList())
               : HistoryOkReply.makeInputHistory(emptyList());
    }

    @Override public Reply doHistoryTail(MessageHeader hdr, boolean output, boolean raw, int number) {
        return output ? HistoryOkReply.makeInOutHistory(emptyList())
                      : HistoryOkReply.makeInputHistory(emptyList());
    }

    @Override
    public Reply doHistorySearch(MessageHeader hdr, boolean output, boolean raw, String pattern, boolean unique) {
        return output ? HistoryOkReply.makeInOutHistory(emptyList())
                      : HistoryOkReply.makeInputHistory(emptyList());
    }

    void handleExecution(ExecuteContext context) throws InterruptedException {
        LOG.debug("execute: begin");
        String result = context.execute();
        LOG.debug("execute: result={}", result);
        if (result != null) {
            LOG.debug("post execute result");
            sender.postIoPubExecuteResult(context.getHeader(), context.getExecutionCount(),
                                          singletonMap("text/plain", result), emptyMap());
        }
        LOG.debug("execute: success");
    }

    @Override public void acceptUserInput(MessageHeader hdr, String value) {
        requireNonNull(value);
        synchronized (userInputMonitor) {
            userInput = value;
            userInputMonitor.notifyAll();
        }
    }

    static class Holder extends ExecuteContext {
        Holder(AbstractKernel kernel, Sender sender, String engineId, int executionCount, MessageHeader header,
               String code, boolean silent, boolean storeHistory, Map<String, String> userExpressions,
               boolean allowStdin)
        {
            super(kernel, sender, engineId, executionCount, header, code, silent, storeHistory, userExpressions,
                  allowStdin);
        }

        @Override public String execute() throws InterruptedException {
            return null;
        }
    }
}
