package tenet.jupyter.v5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static java.lang.Runtime.getRuntime;

/**
 * The multithreaded variant of the AbstractKernel puts code execution in a thread pool.
 * Other operations are either quick enough you should be able to deal with them without a thread pool,
 * or they are dependent on user input anyway.
 *
 * <p>The preexecution is performed in the main kernel thread. This is to allow a language
 * to inspect execute requests for bindings in the same order the user requested.
 *
 * @see ExecuteContext#preExecute()  Executed in the main thread
 * @see ExecuteContext#execute() Executed in the thread pool; same object
 */
public abstract class MultithreadedKernel extends AbstractKernel {

    protected int getNormalPoolSize() {
        return 2;
    }

    protected int getMaxPoolSize() {
        return 2 * getRuntime().availableProcessors();
    }

    @Override protected ExecutorService makeThreadPool() {
        return new ThreadPoolExecutor(getNormalPoolSize(), getMaxPoolSize(),
                                      30L, TimeUnit.SECONDS,
                                      new LinkedBlockingQueue<>());
    }
}
