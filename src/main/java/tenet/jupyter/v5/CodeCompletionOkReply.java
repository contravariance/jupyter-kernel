package tenet.jupyter.v5;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * A response to a code completion request.
 */
public class CodeCompletionOkReply extends Reply {
    public CodeCompletionOkReply(Collection<String> matches, int cursorStart, int cursorEnd,
                                 Map<String, Object> metadata) {
        super(Status.ok);
        this.matches = matches;
        this.cursorStart = cursorStart;
        this.cursorEnd = cursorEnd;
        this.metadataPart = metadata;
    }

    public Collection<String> matches;
    public int cursorStart;
    public int cursorEnd;

    public static CodeCompletionOkReply makeSimple(int start, int end, String... matches) {
        return new CodeCompletionOkReply(Arrays.asList(matches), start, end, Collections.emptyMap());
    }

    public static CodeCompletionOkReply makeNocompletion(int cursorPos) {
        return makeSimple(cursorPos, cursorPos);
    }
}
