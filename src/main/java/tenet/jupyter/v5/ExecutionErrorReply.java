package tenet.jupyter.v5;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * Standard interface indicating an execution error.
 */
public class ExecutionErrorReply extends ErrorReply {
    public int executionCount;

    public ExecutionErrorReply(int executionCount) {
        this.executionCount = executionCount;
    }

    public ExecutionErrorReply(int executionCount, String issue, String details, Collection<String> trace) {
        super(issue, details, trace);
        this.executionCount = executionCount;
    }

    public ExecutionErrorReply(int executionCount, Throwable thrw) {
        super(thrw);
        this.executionCount = executionCount;
    }

    public static ExecutionErrorReply makeSimple(int executionCount, Throwable throwable) {
        return new ExecutionErrorReply(executionCount, throwable);
    }

    public static ExecutionErrorReply makeSimple(int executionCount, String issue, String details, String... trace) {
        return new ExecutionErrorReply(executionCount, issue, details, asList(trace));
    }
}
