package tenet.jupyter.v5;

/**
 * All replies have some kind of status and optional metadataPart.
 */
public class Reply extends Message {
    public Reply() {
    }

    public Reply(Status status) {
        this.status = status;
    }

    public Status status;

    public enum Status {
        ok, error, // Standard status codes
        complete, incomplete, invalid, unknown // Only for use with IsCompleteReply.
    }
}
