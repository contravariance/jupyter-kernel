package tenet.jupyter.v5;

import java.util.Collections;
import java.util.Map;

/**
 * The user may specify metadataPart to send with the message.
 */
public class Message {
    /**
     * This is a bit confusing. A message on the wire is transmitted in various parts,
     * header, parent header, metadata and content. The subclasses of Message are concerned
     * with the content portion, and the content portion may include a field named metadata.
     *
     * <p>This field is used to populate the metadata part of the message, which is entirely
     * independent of the normal content. What it actually does probably depends on the client.
     */
    public transient Map<String, Object> metadataPart = Collections.emptyMap();
}
