/**
 * These classes closely track the version 5 of the jupyter wire protocol.
 * A kernel should implement these classes, starting with {@link tenet.jupyter.v5.Kernel}
 * and working out from there. To actually run the kernel, you need a runner.
 *
 * <h3>Forward compatibility</h3>
 *
 * <p>The plan is that if a wire protocol v6 comes out, the runner would be updated to handle
 * it, and it would Just Work with a v5 kernel.
 */
package tenet.jupyter.v5;
