package tenet.jupyter.v5;

import java.util.Collections;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Response to inspect.
 * @see <a href="http://jupyter-client.readthedocs.io/en/latest/messaging.html#introspection">Relevant Jupyter docs</a>
 */
public class IntrospectionOkReply extends Reply {
    public IntrospectionOkReply() {
        super(Status.ok);
    }

    /**
     * The data dict contains key/value pairs, where the keys are MIME
     * types and the values are the raw data of the representation in that
     * format. A map of MIME types to actual data.
     */
    public Map<String, byte[]> data = Collections.emptyMap();

    /**
     * Indicate whether the response found anything.
     * True if data was found.
     */
    public boolean found = false;

    /**
     * Construct a response indicating nothing found.
     * @return an introspection response
     */
    public static IntrospectionOkReply makeNotFound() {
        return new IntrospectionOkReply();
    }

    /**
     * Construct a plain text response.
     * @param mime the mime type
     * @param data data to embed in the response
     * @return an introspection response
     */
    public static IntrospectionOkReply makePlainText(String mime, String data) {
        IntrospectionOkReply reply = new IntrospectionOkReply();
        reply.data = Collections.singletonMap("text/plain", data.getBytes(UTF_8));
        return reply;
    }
}
