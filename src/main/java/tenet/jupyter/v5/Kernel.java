package tenet.jupyter.v5;

import java.util.Map;

/**
 * The Runner that provides events to your kernel is single-threaded. Any request may be responded to with null, and
 * you can send a response whenever you have the answer via the sender.
 *
 * <p>Besides setting up classes so that the responses are correctly structured, this interface and the
 * support classes don't do much to ensure that a kernel implementation follows the correct protocol.
 * @see tenet.jupyter.runner.RunnerV5 The entry point for this kernel.
 * @see AbstractKernel An implementation that runs code execution in a thread pool.
 */
public interface Kernel {
    /**
     * Received from the runner when the kernel is initiated.
     * @param sender the kernel may use this to post messages to the frontend.
     * @param engineId the engine ID (also refered to as session)
     * @param args any additional arguments passed to the runner.
     */
    void doStartUp(Sender sender, String engineId, Map<String, String> args);

    /**
     * Received from the frontend to terminate the process.
     * @param hdr the header of the shutdown message request
     */
    void doShutdown(MessageHeader hdr);

    /**
     * Requests details about the kernel.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @return the info about the kernel, or null if the kernel will respond asynchronously
     */
    KernelInfo getKernelInfo(MessageHeader hdr);

    /**
     * Requests that the kernel handle an execute request. These notes are extracted from the docs.
     *
     * <p><tt>silent</tt>: silent=True forces store_history to be False, and will <b>not</b>:
     * <ul><li> broadcast output on the IOPUB channel
     * <li> have an execute_result</ul>
     * The default is False.
     *
     * <p><tt>store_history</tt>: The default is True if silent is False.  If silent is True, store_history
     * is forced to be False.
     *
     * <p><tt>user_expression</tt>: The rich display-data representation of each will be evaluated after execution.
     * See the display_data content for the structure of the representation data.
     *
     * <p><tt>allow_stdin</tt>: If this is true, code running in the kernel can prompt the user for input
     * with an input_request message (see below). If it is false, the kernel should not send these messages.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param code Source code to be executed by the kernel, one or more lines.
     * @param silent A boolean flag which, if True, signals the kernel to execute this code as quietly as possible
     * @param storeHistory A boolean flag which, if True, signals the kernel to populate history
     * @param userExpressions A dict mapping names to expressions to be evaluated in the user's dict
     * @param allowStdin Some frontends do not support stdin requests.
     * @return an {@link ExecutionOkReply} or {@link ExecutionErrorReply}, or null for asynchronous response
     */
    Reply doExecute(MessageHeader hdr, String code, boolean silent, boolean storeHistory,
                             Map<String, String> userExpressions, boolean allowStdin);

    /**
     * Requests code completion.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param code the line of code to complete
     * @param pos the cursor position within the line
     * @return a {@link CodeCompletionOkReply} or {@link ErrorReply}, or null for asynchronous response
     */
    Reply doComplete(MessageHeader hdr, String code, int pos);

    /**
     * As opposed to {@link #doComplete}, this only checks whether the user needs to be prompted for more code.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param code the line of code that might or might not be complete
     * @return a reply to indicate whether a prompt is needed, or null to respond asynchronously
     */
    IsCompleteReply doIsComplete(MessageHeader hdr, String code);

    /**
     * Requests introspection, aka tab completion.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param code the code to introspect
     * @param cursorPos the cursor location
     * @param detailLevel whether to request more or less detail
     * @return an {@link IntrospectionOkReply} or {@link ErrorReply}, or null for asynchronous response
     */
    Reply doInspect(MessageHeader hdr, String code, int cursorPos, DetailLevel detailLevel);

    /**
     * Requests a range of history cells.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param output true if outputs should be included
     * @param raw true for raw input, false for transformed
     * @param session a positive session number, or a negative number to count back from the current session.
     * @param start a line number within the session
     * @param stop a line number within the session
     * @return a {@link HistoryOkReply} or {@link ErrorReply}, or null for asynchronous response
     */
    Reply doHistoryRange(MessageHeader hdr, boolean output, boolean raw, int session, int start, int stop);

    /**
     * Requests the last `number` history cells.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param output true if outputs should be included
     * @param raw true for raw input, false for transformed
     * @param number the number of lines to return
     * @return a {@link HistoryOkReply} or {@link ErrorReply}, or null for asynchronous response
     */
    Reply doHistoryTail(MessageHeader hdr, boolean output, boolean raw, int number);

    /**
     * Request a search of history cells that match the pattern.
     * @param hdr the header of the message, used to construct an asynchronous reply.
     * @param output true if outputs should be included
     * @param raw true for raw input, false for transformed
     * @param pattern get cells matching the specified glob pattern (with * and ? as wildcards).
     * @param unique if true, do not include duplicated history.
     * @return a {@link HistoryOkReply} or {@link ErrorReply}, or null for asynchronous response
     */
    Reply doHistorySearch(MessageHeader hdr, boolean output, boolean raw, String pattern, boolean unique);

    /**
     * After the kernel has submitted an input request to the frontend, this method will be called with the response.
     * @param hdr the header of the request the kernel made originally.
     * @param value the text the user entered.
     * @see Sender#postStdinRequest(MessageHeader, String)
     * @see Sender#postStdinPasswordRequest(MessageHeader, String)
     */
    void acceptUserInput(MessageHeader hdr, String value);

    /**
     * The level of detail requested in introspection.
     */
    enum DetailLevel {
        less, more
    }
}
