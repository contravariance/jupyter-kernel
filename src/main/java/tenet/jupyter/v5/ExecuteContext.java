package tenet.jupyter.v5;

import java.time.Clock;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.exceptions.KernelException;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;

/**
 * This class provides some convenience methods to use the kernel more effectively.
 *
 * <p>In particular, reading and writing stdin and stdout have straightforward synchronous methods.
 */
public abstract class ExecuteContext {
    private final AbstractKernel kernel;
    private final Sender sender;
    private final int executionCount;
    private final MessageHeader header;
    private final String code;
    private final boolean silent;
    private final boolean storeHistory;
    private final boolean allowStdin;
    private boolean dependenciesMet;
    private final Instant started;
    private final Map<String, Object> meta;

    boolean inWorkerThread;
    Reply executeReply;

    private final static Logger LOG = LoggerFactory.getLogger(ExecuteContext.class);
    static Clock clock = Clock.systemDefaultZone();

    protected ExecuteContext(ExecuteContext ec) {
        kernel = ec.kernel;
        sender = ec.sender;
        executionCount = ec.executionCount;
        header = ec.header;
        code = ec.code;
        silent = ec.silent;
        storeHistory = ec.storeHistory;
        allowStdin = ec.allowStdin;
        dependenciesMet = true;
        started = ec.started;
        meta = ec.meta;
        executeReply = ec.executeReply;
    }

    ExecuteContext(AbstractKernel kernel, Sender sender, String engineId, int executionCount,
                   MessageHeader header, String code, boolean silent, boolean storeHistory,
                   Map<String, String> userExpressions, boolean allowStdin) {
        this.kernel = kernel;
        this.sender = sender;
        this.executionCount = executionCount;
        this.header = header;
        this.code = code;
        this.silent = silent;
        this.storeHistory = storeHistory;
        this.allowStdin = allowStdin;
        this.meta = new HashMap<>();
        dependenciesMet = true;
        started = Instant.now(clock);
        executeReply = new ExecutionOkReply(executionCount, userExpressions);
    }

    /**
     * An optional execution handler, this is mostly for brief inspection of the code
     * and is called in the kernel thread. It can't return a value, but it can raise
     * an execption to fail.
     */
    public void preExecute() {}

    /**
     * The main execution handler, this should handle the main work of code execution.
     * @return a plain text result to post, or null
     * @throws InterruptedException implementations should allow the pool to shut down threads through interruptions.
     * @see ExecuteContext#writeResult(String, String) to return a complex result
     * @see #preExecute() Override to do work in the main kernel thread.
     */
    public abstract String execute() throws InterruptedException;

    /**
     * Reply field.
     * @return the execution count, based on the kernel's counter.
     */
    public final int getExecutionCount() {
        return executionCount;
    }
    /**
     * Request field.
     * @return the header of the execute request.
     */
    public final MessageHeader getHeader() {
        return header;
    }
    /**
     * Request field.
     * @return the code to execute.
     */
    public final String getCode() {
        return code;
    }
    /**
     * Request field.
     * @return if true, signals the kernel to execute this code as quietly as possible.
     */
    public final boolean isSilent() {
        return silent;
    }
    /**
     * Request field.
     * @return if true, signals the kernel to populate history.
     */
    public final boolean isStoreHistory() {
        return storeHistory;
    }

    /**
     * Request field.
     * @return if true, the kernel may make stdin requests.
     * @see Sender#postStdinRequest(MessageHeader, String)
     */
    public final boolean isAllowStdin() {
        return allowStdin;
    }

    /**
     * Sets that dependencies are met in the reply that will be posted to the client.
     * @param newValue true to indicate dependencies are met.
     */
    public final void setDependenciesMet(boolean newValue) {
        dependenciesMet = newValue;
    }

    /**
     * Posts text to the stdout stream.
     * @param data text to post to stdout.
     */
    public final void writeStdout(String data) {
        sender.postIoPubStdout(header, data);
    }
    /**
     * Posts text to the stderr stream.
     * @param data text to post to stderr.
     */
    public final void writeStderr(String data) {
        sender.postIoPubStderr(header, data);
    }
    /**
     * Prompt for stdin and wait on a prompt.
     * @param prompt a prompt to display to the user
     * @return the text the user entered
     * @throws InterruptedException interruptions most likely indicate the pool is shutting down
     */
    public final String promptStdin(String prompt) throws InterruptedException {
        checkStdinAllowed();
        MessageHeader sent = sender.postStdinRequest(header, prompt);
        return waitForStdinResponse();
    }
    /**
     * Prompt for masked stdin and wait on a prompt.
     * @param prompt a prompt to display to the user
     * @return the password the user entered
     * @throws InterruptedException interruptions most likely indicate the pool is shutting down
     */
    public final String promptStdinPassword(String prompt) throws InterruptedException {
        checkStdinAllowed();
        MessageHeader sent = sender.postStdinPasswordRequest(header, prompt);
        return waitForStdinResponse();
    }

    void checkStdinAllowed() {
        if (!isAllowStdin())
            throw new KernelException("Attempted to prompt for stdin when client doesn't allow it");
        if (!this.inWorkerThread)
            throw new KernelException("Attempted to prompt for stdin in preexecute method");
    }

    String waitForStdinResponse() throws InterruptedException {
        synchronized (kernel.userInputMonitor) {
            kernel.userInputMonitor.wait();
            return kernel.userInput;
        }
    }

    /**
     * Posts a result to be associated with this code execution.
     * @see #execute() if the override returns a string, this is called for you.
     * @param mimetype a mimetype of the result data
     * @param value the actual result; use base64 to encode a binary
     */
    public final void writeResult(String mimetype, String value) {
        writeResult(singletonMap(mimetype, value));
    }

    /**
     * Posts a result to be associated with this code execution.
     * @param resultMap  maps mimetypes to result data; use base64 to encode a binary
     */
    public final void writeResult(Map<String, String> resultMap) {
        sender.postIoPubExecuteResult(header, executionCount, resultMap, emptyMap());
    }

    /**
     * Replaces the ExecutionOkReply with an error reply object.
     */
    public final void setExecuteStatusError() {
        if (!(this.executeReply instanceof ExecutionErrorReply)) {
            this.executeReply = new ExecutionErrorReply(executionCount);
        }
    }

    /**
     * Replaces the ExecutionOkReply with an error reply object.
     * @param exc an execption to respond with
     */
    public final void setExecuteStatusError(Throwable exc) {
        this.executeReply = new ExecutionErrorReply(executionCount, exc);
    }

    /**
     * Replaces the ExecutionOkReply with an error reply object.
     * @param issue the name of an issue to raise (exception class)
     * @param details the details of an issue (exception message)
     * @param trace the stack trace
     */
    public final void setExecuteStatusError(String issue, String details, Collection<String> trace) {
        this.executeReply = new ExecutionErrorReply(executionCount, issue, details, trace);
    }

    /**
     * Gets the Reply object.
     * @return the reply object that will be sent when the execution is complete.
     */
    public final Reply getExecutionReply() {
        return this.executeReply;
    }

    /**
     * Get the time the execution request started.
     * @return the instant this context object was created
     */
    public final Instant getStarted() {
        return started;
    }

    /**
     * Get the metadata map for the Execution reply.
     * This will be pre-populated with the settings Jupyter needs.
     * @return the Execution metadata.
     */
    public final Map<String,Object> getMetadata() {
        if (meta.isEmpty()) {
            meta.put("engine", kernel.engineId);
            meta.put("started", started);
        }
        meta.put("dependencies_met", dependenciesMet);
        meta.put("status", executeReply.status);
        return meta;
    }

    /**
     * This can be overridden to post a traceback to stderr when an execution has failed.
     */
    protected void writeTracebackToConsole(ExecutionErrorReply reply) {
        LOG.warn("post execute: traceback={}({})", reply.exceptionName, reply.exceptionValue);
        this.writeStderr(reply.toString());
    }

    final void postExecuteReply() {
        LOG.debug("post execute");
        Reply reply = executeReply;
        reply.metadataPart = getMetadata();
        if (reply instanceof ExecutionOkReply) {
            LOG.debug("post execution ok");
            sender.postShellExecution(header, (ExecutionOkReply) reply);
        } else {
            if (!(reply instanceof ExecutionErrorReply)) {
                reply = ExecutionErrorReply.makeSimple(executionCount, "Invalid class for reply",
                                                       reply.getClass().getName() + " unexpected");
            }
            try {
                this.writeTracebackToConsole((ExecutionErrorReply) reply);
            } catch (RuntimeException | Error exc) {
                LOG.error("Error writing traceback", exc);
            }
            sender.postShellExecution(header, (ExecutionErrorReply) reply);
        }
    }
}
