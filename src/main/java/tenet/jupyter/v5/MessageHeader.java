package tenet.jupyter.v5;

import java.time.Instant;

import com.google.gson.annotations.SerializedName;

/**
 * The header identifies the source of the request. As this is an opaque type, it's provided as an interface.
 */
public interface MessageHeader {
    @SerializedName("msg_id")
    String getMessageId();

    @SerializedName("username")
    String getUsername();

    @SerializedName("session")
    String getSession();

    @SerializedName("date")
    Instant getDate();

    @SerializedName("msg_type")
    String getMessageType();

    @SerializedName("version")
    String getProtocolVersion();
}
