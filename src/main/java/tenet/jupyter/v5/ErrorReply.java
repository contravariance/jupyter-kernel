package tenet.jupyter.v5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * A reply message with an error status. You can just send this; there are specific interfaces for
 * the sake of completeness.
 */
public class ErrorReply extends Reply {
    public ErrorReply(String issue, String details, Collection<String> trace) {
        super(Status.error);
        this.exceptionName = issue;
        this.exceptionValue = details;
        this.traceback = trace;
    }

    public ErrorReply() {
        super(Status.error);
    }

    public ErrorReply(Throwable thrw) {
        this(thrw.getClass().getSimpleName(), thrw.getMessage(), frames(thrw));
    }

    public String exceptionName;
    public String exceptionValue;
    public Collection<String> traceback;

    @Override public String toString() {
        StringBuilder sb = new StringBuilder();
        String excName = this.exceptionName;
        String excValue = this.exceptionValue;
        Collection<String> tb = this.traceback;
        if (!tb.isEmpty()) {
            sb.append("Traceback (recent calls last):\n");
            for (String frame : tb) {
                sb.append("  ").append(frame).append('\n');
            }
        }
        sb.append(exceptionName).append(": ").append(exceptionValue);
        return sb.toString();
    }

    private static List<String> frames(Throwable throwable) {
        StackTraceElement[] frames = throwable.getStackTrace();
        List<String> trace = new ArrayList<>(frames.length);
        for(StackTraceElement element : frames) {
            trace.add(element.toString());
        }
        Collections.reverse(trace); // Java stack traces are backwards compared to python.
        return trace;
    }

    public static ErrorReply makeException(Throwable thrw) {
        return new ErrorReply(thrw);
    }

    public static ErrorReply makeSimple(String issue, String details, String... trace) {
        return new ErrorReply(issue, details, Arrays.asList(trace));
    }
}
