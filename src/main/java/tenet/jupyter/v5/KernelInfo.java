package tenet.jupyter.v5;

import java.util.Arrays;
import java.util.Collection;

/**
 * Reports kernel info when requested by the server. Anticipated that the implementation is a class that returns static
 * strings. All of these appear to be required.
 *
 * @see <a href="https://jupyter-client.readthedocs.io/en/latest/messaging.html#msging-kernel-info">Source docs</a>
 * @see Kernel#getKernelInfo(MessageHeader)
 */
public class KernelInfo extends Message {
    /**
     * Standard constructor.
     * @param banner banner of information about the kernel
     * @param implementation the kernel implementation name
     * @param implementationVersion implementation version number
     * @param languageInfo information about the language the kernel implements
     * @param helpLinks a collection of urls to get help
     */
    public KernelInfo(String banner, String implementation, String implementationVersion,
                      LanguageInfo languageInfo, Collection<HelpLink> helpLinks) {
        super();
        this.banner = banner;
        this.implementation = implementation;
        this.implementationVersion = implementationVersion;
        this.languageInfo = languageInfo;
        this.helpLinks = helpLinks;
    }

    /**
     * A banner of information about the kernel, which may be desplayed in console environments.
     */
    public String banner;

    /**
     * The kernel implementation name (e.g. 'ipython' for the IPython kernel)
     */
    public String implementation;

    /**
     * Implementation version number; three dotted numbers.
     * The version number of the kernel's implementation
     * (e.g. {@code IPython.__version__} for the IPython kernel)
     */
    public String implementationVersion;

    /**
     * The protocol version. This must be '5.0' for the v5 packages to work.
     */
    public String protocolVersion = "5.0";

    /**
     * Information about the language the kernel implements.
     */
    public LanguageInfo languageInfo;

    /**
     * An optional list of dictionaries, each with keys 'text' and 'url'.
     * These will be displayed in the help menu in the notebook UI.
     */
    public Collection<HelpLink> helpLinks;

    /**
     * A link to documentation.
     */
    public static class HelpLink {
        /**
         * Constructs a null link. The text and url should be assigned after construction.
         */
        public HelpLink() {}

        /**
         * Standard constructor.
         * @param text The text label for the help link.
         * @param url The URL linking to the help docs.
         */
        public HelpLink(String text, String url) {
            this.text = text;
            this.url = url;
        }

        /**
         * The text label for the help link.
         */
        public String text;
        /**
         * The URL linking to the help docs.
         */
        public String url;
    }

    /**
     * Convenience factory method.
     * @param banner banner of information about the kernel
     * @param implementation the kernel implementation name
     * @param implementationVersion implementation version number
     * @param languageInfo information about the language the kernel implements
     * @param helpLinks urls to get help
     * @return the constructed instance
     */
    public static KernelInfo makeSimple(String banner, String implementation, String implementationVersion,
                                        LanguageInfo languageInfo, HelpLink... helpLinks) {
        return new KernelInfo(banner, implementation, implementationVersion, languageInfo,
                              Arrays.asList(helpLinks));
    }
}
