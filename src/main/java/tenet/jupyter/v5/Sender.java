package tenet.jupyter.v5;

import java.util.Map;

/**
 * An interface that sends replies. The constructed runner implements Sender.
 */
public interface Sender {
    /**
     * Posts a code completed reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellCodeCompleted(MessageHeader token, CodeCompletionOkReply reply);
    /**
     * Posts a code completed reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellCodeCompleted(MessageHeader token, ErrorReply reply);

    /**
     * Posts a code completed reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellExecution(MessageHeader token, ExecutionOkReply reply);
    /**
     * Posts a code completed reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellExecution(MessageHeader token, ExecutionErrorReply reply);

    /**
     * Posts a code completed reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellHistory(MessageHeader token, HistoryOkReply reply);
    /**
     * Posts a code completed reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellHistory(MessageHeader token, ErrorReply reply);

    /**
     * Posts an inspection reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellIntrospection(MessageHeader token, IntrospectionOkReply reply);
    /**
     * Posts an inspection reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellIntrospection(MessageHeader token, ErrorReply reply);

    /**
     * Posts an IsComplete reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply a reply
     */
    void postShellIsComplete(MessageHeader token, IsCompleteReply reply);

    /**
     * Posts a KernelInfo reply to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param reply information about the kernel
     */
    void postShellKernelInfo(MessageHeader token, KernelInfo reply);

    /**
     * Posts stdout data to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param data arbitrary text to display on the STDOUT stream
     */
    void postIoPubStdout(MessageHeader token, String data);
    /**
     * Posts stderr data to the frontend.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param data arbitrary text to display on the STDERR stream
     */
    void postIoPubStderr(MessageHeader token, String data);

    /**
     * Indicates to the client the current execution state of the kernel.
     * @param token typically the token from the doExecute request that affected kernel state.
     * @param state the new state, not null.
     */
    void postIoPubStatus(MessageHeader token, ExecutionState state);
    /**
     * Report on the io_pub topic that execution is handling code.
     * @param token typically the token from the doExecute request that is being handled.
     * @param executionCount the execution count.
     * @param code the code being executed.
     */
    void postIoPubExecuteInput(MessageHeader token, int executionCount, String code);
    /**
     * Report on the io_pub topic the result of executing the code.
     * @param token typically the token from the doExecute request that affected kernel state.
     * @param executionCount the execution count.
     * @param data the data to post
     * @param metadata the metadataPart to post
     */
    void postIoPubExecuteResult(MessageHeader token, int executionCount, Map<String, String> data,
                                Map<String, Object> metadata);

    /**
     * This type of message is used to bring back data that should be displayed (text, html, svg, etc.) in the
     * frontends. This data is published to all frontends. Each message can have multiple representations of the data;
     * it is up to the frontend to decide which to use and how. A single message should contain all possible
     * representations of the same information. Each representation should be a JSONable data structure, and should
     * be a valid MIME type.
     *
     * <p>The data dict contains key/value pairs, where the keys are MIME
     * types and the values are the raw data of the representation in that
     * format. A map of MIME types to actual data.
     *
     * <p>The metadata contains any metadata that describes the output. Global keys are assumed to apply to the output
     * as a whole. The metadata dict can also contain mime-type keys, which will be sub-dictionaries, which are
     * interpreted as applying only to output of that type. Third parties should put any data they write into a single
     * dict with a reasonably unique name to avoid conflicts.
     *
     * <p>The only metadata keys currently defined in IPython are the width and height of images:
     * <pre>
     * metadata = {
     *   'image/png' : {
     *     'width': 640,
     *     'height': 480
     *   }
     * }
     * </pre>
     *
     * @param token typically the token from the doExecute request that affected kernel state.
     * @param data the data to post
     * @param metadata the metadataPart to post
     */
    void postIoPubDisplayData(MessageHeader token, Map<String, String> data, Map<String, Object> metadata);

    /**
     * Prompts the user to enter plain text data.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param prompt a prompt to display to the user on the frontend.
     * @return a token identifying this request
     * @see Kernel#acceptUserInput the kernel will respond asynchronously
     */
    MessageHeader postStdinRequest(MessageHeader token, String prompt);

    /**
     * Prompts the user to enter masked data.
     * @param token the token identifying to the frontend which message this is a reply to
     * @param prompt a prompt to display to the user on the frontend.
     * @return a token identifying this request
     * @see Kernel#acceptUserInput the kernel will respond asynchronously
     */
    MessageHeader postStdinPasswordRequest(MessageHeader token, String prompt);

    enum ExecutionState {
        idle,
        busy,
        starting
    }
}
