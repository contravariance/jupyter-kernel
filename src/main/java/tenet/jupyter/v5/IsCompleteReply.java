package tenet.jupyter.v5;

/**
 * Indicates whether code requires a continuation prompt.
 * @see <a href="https://jupyter-client.readthedocs.io/en/latest/messaging.html#code-completeness">Jupyter docs</a>
 */
public class IsCompleteReply extends Reply {
    public IsCompleteReply(Status status, String indent) {
        super(status);
        this.indent = indent;
    }

    public IsCompleteReply() {
        super();
    }

    public IsCompleteReply(Status status) {
        super(status);
    }

    /**
     * If status is 'incomplete', indent should contain the characters to use
     * to indent the next line. This is only a hint: frontends may ignore it
     * and use their own autoindentation rules. For other statuses, this
     * field does not exist. Should be the indent string, or null.
     */
    public String indent;

    /**
     * Convenience method to construct a 'complete' reply.
     * @return a constructed reply.
     */
    public static IsCompleteReply complete() {
        return new IsCompleteReply(Status.complete);
    }

    /**
     * Convenience method to construct a 'incomplete' reply.
     * @param prompt the continuation prompt
     * @return a constructed reply.
     */
    public static IsCompleteReply incomplete(String prompt) {
        return new IsCompleteReply(Status.incomplete, prompt);
    }
    /**
     * Convenience method to construct a 'invalid' reply.
     * @return a constructed reply.
     */
    public static IsCompleteReply invalid() {
        return new IsCompleteReply(Status.invalid);
    }
    /**
     * Convenience method to construct a 'unknown' reply.
     * @return a constructed reply.
     */
    public static IsCompleteReply unknown() {
        return new IsCompleteReply(Status.unknown);
    }
}
