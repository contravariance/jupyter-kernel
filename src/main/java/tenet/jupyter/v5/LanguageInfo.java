package tenet.jupyter.v5;

/**
 * Specific details about the language. As with kernel info, docs are copied straight from the official source, so if
 * they're a bit confusing, sorry about that.
 * @see KernelInfo
 * @see Kernel#getKernelInfo(MessageHeader)
 */
public class LanguageInfo {
    public LanguageInfo() {
    }

    public LanguageInfo(String name, String version, String mimetype, String fileExtension, String pygmentsLexer,
                        String codemirrorMode, String nbconvertExporter) {
        this.name = name;
        this.version = version;
        this.mimetype = mimetype;
        this.fileExtension = fileExtension;
        this.pygmentsLexer = pygmentsLexer;
        this.codemirrorMode = codemirrorMode;
        this.nbconvertExporter = nbconvertExporter;
    }

    /**
     * Short name of the programming language that the kernel implements.
     *
     * <p>Example: Kernel included in IPython returns '{@code python}'.
     */
    public String name;

    /**
     * Language version number; three dotted integers.
     *
     * <p>Example: It is a Python version number such as '{@code 2.7.3}' for the kernel included in IPython.
     */
    public String version;

    /**
     * Mimetype for script files in this language. Typically {@code text/your-language-mimetype}.
     */
    public String mimetype;

    /**
     * Extension including the dot, for example, '{@code .py}'. The (one) extension for your language's files.
     */
    public String fileExtension;

    /**
     * Pygments lexer, for highlighting. Probably a short name from the list of lexers, only needed if it differs from
     * the {@link #name} field.  The notebook will complain if this isn't recognized on workbook validation.
     *
     * <p>You can look these up with:
     * <pre>python3
     * import pygments
     * for display, codes, extensions, mimes in pygments.lexers.get_all_lexers():
     *     print("%s = %s" % (codes[0], display))
     * </pre>
     * @see <a href="http://pygments.org/docs/lexers/">List of pygments lexers</a>
     */
    public String pygmentsLexer;

    /**
     * Codemirror mode, for handling editing in the notebook. The notebook will complain if this isn't
     * recognized on workbook validation.
     *
     * <p>The wire protocol docs say this accepts a string or a map. This implementation only supports a string.
     * @see <a href="http://codemirror.net/mode/index.html">List of language modes</a>
     * @see <a href="https://github.com/codemirror/CodeMirror/blob/master/mode/meta.js">Actual mode strings in source</a>
     */
    public String codemirrorMode;

    /**
     * Nbconvert exporter, if notebooks written with this kernel should
     * be exported with something other than the general 'script'
     * exporter. An exporter name, null is allowed.
     */
    public String nbconvertExporter;
}
