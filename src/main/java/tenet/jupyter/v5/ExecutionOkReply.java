package tenet.jupyter.v5;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A reply to indicate a successful execution.
 *
 * <p>Note that the wire protocol allows the deprecated payload field, but this
 * library is not supporting it.
 * @see <a href="http://jupyter-client.readthedocs.io/en/latest/messaging.html#execution-results">Jupyter docs.</a>
 */
public class ExecutionOkReply extends Reply {
    /**
     * Standard constructor.
     * @param executionCount the counter identifying the execution that was completed.
     * @param userExpressions map names to expressions.
     */
    public ExecutionOkReply(int executionCount, Map<String, String> userExpressions) {
        super(Status.ok);
        this.executionCount = executionCount;
        this.userExpressions = userExpressions;
    }

    /**
     * The user expressions in this reply.
     */
    public Map<String, String> userExpressions;
    /**
     * The global kernel counter that increases by one with each request that
     * stores history.  This will typically be used by clients to display
     * prompt numbers to the user.  If the request did not store history, this will
     * be the current value of the counter in the kernel.
     *
     * <p>The kernel should have a single, monotonically increasing counter of all execution requests that are made
     * with {code store_history=True}. This counter is used to populate the {code In[n]} and {code Out[n]} prompts.
     * The value of this counter will be returned as the {code execution_count} field of all
     * {code execute_reply} and {code execute_input} messages.
     */
    public int executionCount;
    /**
     * Payloads are deprecated, but there's no replacement, so this is represented as an opaque data structure
     * so a source fix can be implemented.
     */
    final List<Payload> payload = new ArrayList<>(0);
    public void addPagePayload(Map<String, Object> mimeBundle, int start) {
        PagePayload item = new PagePayload();
        item.source = "page";
        item.data = mimeBundle;
        item.start = start;
        this.payload.add(item);
    }
    public void addSetNextInputPayload(String text, boolean replace) {
        SetNextInputPayload item = new SetNextInputPayload();
        item.source = "set_next_input";
        item.text = text;
        item.replace = replace;
        this.payload.add(item);
    }
    public void addEditPayload(String filename, int lineNumber) {
        EditPayload item = new EditPayload();
        item.source = "edit";
        item.filename = filename;
        item.lineNumber = lineNumber;
        this.payload.add(item);
    }
    public void addAskExitPayload(boolean keepkernel) {
        AskExitPayload item = new AskExitPayload();
        item.source = "ask_exit";
        item.keepkernel = keepkernel;
        this.payload.add(item);
    }

    private static class Payload {
        String source;
    }
    private static class PagePayload extends Payload {
        Map<String, Object> data;
        int start;
    }
    private static class SetNextInputPayload extends Payload {
        String text;
        boolean replace;
    }
    private static class EditPayload extends Payload {
        String filename;
        int lineNumber;
    }
    private static class AskExitPayload extends Payload {
        boolean keepkernel;
    }
}
