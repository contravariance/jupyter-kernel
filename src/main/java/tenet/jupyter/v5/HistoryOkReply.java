package tenet.jupyter.v5;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * Reply to requests for history.
 *
 * <p>A list of 3 tuples, either:
 * <ol><li>{@code (session, line_number, input)} or
 * <li>{@code (session, line_number, (input, output))},</ol>
 * depending on whether output was False or True, respectively.
 */
public class HistoryOkReply extends Reply {
    public HistoryOkReply() {
        super(Status.ok);
    }

    Collection<? extends InputEntry> history = Collections.emptyList();

    public static HistoryOkReply makeInputHistory(Collection<InputEntry> entries) {
        HistoryOkReply reply = new HistoryOkReply();
        reply.history = entries;
        return reply;
    }

    public static HistoryOkReply makeInOutHistory(Collection<IOEntry> entries) {
        HistoryOkReply reply = new HistoryOkReply();
        reply.history = entries;
        return reply;
    }

    public static class InputEntry {
        public int session;
        public int lineNumber;
        public String input;
        public InputEntry() {}

        public InputEntry(int session, int lineNumber, String input) {
            this.session = session;
            this.lineNumber = lineNumber;
            this.input = Objects.toString(input, "");
        }
    }

    public static class IOEntry extends InputEntry {
        public String output;

        public IOEntry() {
        }

        public IOEntry(int session, int lineNumber, String input, String output) {
            super(session, lineNumber, input);
            this.output = output;
        }
    }
}
