/**
 * The v5 package is a mess of interfaces the kernel implementor should implement.
 *
 * <h2>Where to start</h2>
 *
 * <p>The fundamental interface is {@link tenet.jupyter.v5.Kernel}, which has a great deal of documentation of the responses the
 * kernel should give.
 *
 * <p>There are a number of pitfalls, though, and {@link tenet.jupyter.v5.AbstractKernel} is more heavily fleshed out.
 *
 * <h2>Building from source</h2>
 *
 * {@code gradle fatJar} will build {@code build/libs/jupyter-kernel-all-*.jar}.
 *
 * <h2>Running the example ad-hoc</h2>
 *
 * <pre>
 *     java -cp build/libs/jupyter-kernel-all-*.jar tenet.jupyter.runner.RunnerV5 \
 *         --kernel-class tenet.jupyter.example.ExampleKernel
 * </pre>
 *
 * <h2>Installation</h2>
 *
 * <p>To actually run the kernel, the user needs to run {@code jupyter-kernelspec install your/directory/name} with a
 * kernel spec file, kernel.json, in your directory with:
 *
 * <pre>
 * {
 *   "argv": ["java", "-cp", "your classpath",
 *                    "tenet.jupyter.runner.RunnerV5",
 *                    "--kernel-class",
 *                    "my.great.kernel.Kernel",
 *                    "--connection-file",
 *                    "{connection_file}"],
 *   "env": {"CLASSPATH": "Can go here instead of -cp"},
 *   "display_name": "My Great Language",
 *   "language": "great"
 * }
 * </pre>
 *
 * The details are <a href="https://jupyter-client.readthedocs.io/en/latest/kernels.html#kernelspecs">here</a>.
 */
package tenet.jupyter;
