package tenet.jupyter.exceptions;

/**
 * A problem related to configuration.
 */
public class InvalidConfiguration extends KernelException {
    public InvalidConfiguration() {
    }

    public InvalidConfiguration(String message) {
        super(message);
    }

    public InvalidConfiguration(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidConfiguration(Throwable cause) {
        super(cause);
    }
}
