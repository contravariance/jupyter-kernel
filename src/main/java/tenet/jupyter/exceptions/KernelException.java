package tenet.jupyter.exceptions;

/**
 * A problem in the kernel.
 */
public class KernelException extends RuntimeException {
    public KernelException() {
    }

    public KernelException(String message) {
        super(message);
    }

    public KernelException(String message, Throwable cause) {
        super(message, cause);
    }

    public KernelException(Throwable cause) {
        super(cause);
    }
}
