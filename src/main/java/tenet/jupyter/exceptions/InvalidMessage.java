package tenet.jupyter.exceptions;

/**
 * Reflects problems parsing or constructing a message.
 */
public class InvalidMessage extends KernelException {
    public InvalidMessage() {
    }

    public InvalidMessage(String message) {
        super(message);
    }

    public InvalidMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidMessage(Throwable cause) {
        super(cause);
    }
}
