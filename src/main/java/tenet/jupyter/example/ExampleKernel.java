package tenet.jupyter.example;

import java.io.PrintStream;
import java.time.Instant;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tenet.jupyter.io.InstantAdapter;
import tenet.jupyter.v5.AbstractKernel;
import tenet.jupyter.v5.CodeCompletionOkReply;
import tenet.jupyter.v5.ExecuteContext;
import tenet.jupyter.v5.ExecutionOkReply;
import tenet.jupyter.v5.HistoryOkReply;
import tenet.jupyter.v5.IntrospectionOkReply;
import tenet.jupyter.v5.IsCompleteReply;
import tenet.jupyter.v5.KernelInfo;
import tenet.jupyter.v5.KernelInfo.HelpLink;
import tenet.jupyter.v5.LanguageInfo;
import tenet.jupyter.v5.MessageHeader;
import tenet.jupyter.v5.Reply;
import tenet.jupyter.v5.Sender;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;

/**
 * Prints out all the messages it gets and has a bunch of test methods.
 *
 * @see tenet.jupyter the package docs explain how to install this.
 */
public class ExampleKernel extends AbstractKernel {
    private Gson gson = new GsonBuilder()
            .registerTypeAdapter(Instant.class, new InstantAdapter())
            .setPrettyPrinting()
            .create();

    private static final Logger LOG = LoggerFactory.getLogger(ExampleKernel.class);

    private void post(Object... parts) {
        PrintStream ps = System.out;
        ps.print('[');
        String comma = "";
        for(Object part : parts) {
            if (part instanceof MessageHeader || part instanceof Reply || part instanceof Map
                || part instanceof ExecuteContext || part instanceof Collection<?>) {
                ps.append(comma);
                comma = " ";
                gson.toJson(part, ps);
            } else {
                boolean isKey = part instanceof String && ((String) part).endsWith("=");
                ps.print(isKey ? ", " : comma);
                ps.print(part);
                comma = isKey ? "" : " ";
            }
        }
        ps.println(']');
    }

    @Override protected ExecutorService makeThreadPool() {
        ThreadFactory tf = new ThreadFactoryBuilder()
                .setUncaughtExceptionHandler((t, exc) -> LOG.error("Worker failed.", exc))
                .setDaemon(true)
                .build();
        return Executors.newSingleThreadExecutor(tf);
    }

    @Override protected ExampleContext makeExecuteContext(ExecuteContext src) {
        return new ExampleContext(src);
    }

    @Override public void doStartUp(Sender sender, String engineId, Map<String, String> args) {
        post("startup", "engineId=", engineId, "args=", args);
        super.doStartUp(sender, engineId, args);
    }

    @Override public void doShutdown(MessageHeader hdr) {
        post("shutdown", hdr);
    }

    @Override public Reply doExecute(MessageHeader hdr, String code, boolean silent, boolean storeHistory,
                                              Map<String, String> userExpressions, boolean allowStdin) {
        post("execute", "hdr=", hdr,
             "code=", code, "silent=", silent,
             "storeHistory=", storeHistory, "userExpressions=", userExpressions,
             "allowStdin=", allowStdin);
        return super.doExecute(hdr, code, silent, storeHistory, userExpressions, allowStdin);
    }

    private static final String redDot = "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//"
            + "8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";

    @Override public KernelInfo getKernelInfo(MessageHeader hdr) {
        post("kernel info", "hdr=", hdr);
        LanguageInfo info = new LanguageInfo();
        // If the language info is not null, jupyter notebook will validate it. This is the minimal information
        // needed to convince the jupyter notebook to not complain about validation errors.
        info.name = "example-machine";
        info.version = "0.9";
        info.fileExtension = ".example";
        info.codemirrorMode = "Plain Text";
        info.mimetype = "text/plain";
        info.pygmentsLexer = "text";
        return KernelInfo.makeSimple("Example Kernel!", "Implementation?", "1.2.3", info,
                                     new HelpLink("Need help? Just bing it!", "https://bing.com/"));
    }

    private final static Pattern matchWord = Pattern.compile("[a-zA-Z0-9]+$");
    @Override public Reply doComplete(MessageHeader hdr, String code, int pos) {
        post("code complete", "hdr=", hdr, "code=", code, "pos=", pos);
        // pos identifies where in code the cursor is.
        if ("test".startsWith(code)) {
            return CodeCompletionOkReply.makeSimple(0, code.length(),
                                                    "test_hello_world",
                                                    "test_stdin",
                                                    "test_password",
                                                    "test_execute_result",
                                                    "test_display_data",
                                                    "test_return_image",
                                                    "test_two_results",
                                                    "test_code_page");
        }

        Matcher match = matchWord.matcher(code);
        if (match.matches()) {
            int start = match.start();
            int end = match.end() + 3;
            String[] words = new String[3];
            words[0] = match.group() + "Foo";
            words[1] = match.group() + "Bar";
            words[2] = match.group() + "Qux";

            return CodeCompletionOkReply.makeSimple(start, end, words);
        } else {
            return CodeCompletionOkReply.makeSimple(0, -1);
        }

    }

    @Override public IsCompleteReply doIsComplete(MessageHeader hdr, String code) {
        post("is complete", "hdr=", hdr, "code=", code);
        int len = code.length(), parens = 0;
        for (int idx = 0; idx < len; idx++) {
            char c = code.charAt(idx);
            if (c == '(')
                parens++;
            else if (c == ')')
                parens--;
            if (parens < 0)
                return IsCompleteReply.invalid();
        }
        return parens == 0 ? IsCompleteReply.complete() : IsCompleteReply.incomplete(")? :");
    }

    @Override public Reply doInspect(MessageHeader hdr, String code, int cursorPos,
                                                  DetailLevel detailLevel) {
        post("inspect", "hdr=", hdr, "code=", code, "cusrorPos=", cursorPos, "detail=", detailLevel);
        return IntrospectionOkReply.makePlainText("text/plain", "Found something here!");
    }

    @Override public Reply doHistoryRange(MessageHeader hdr, boolean output, boolean raw, int session,
                                                 int start, int stop) {
        post("historyRange", "hdr=", hdr, "output=", output, "raw=", raw, "session=", session,
             "start=", start, "stop=", stop);
        return new HistoryOkReply();
    }

    @Override public Reply doHistoryTail(MessageHeader hdr, boolean output, boolean raw, int number) {
        post("historyTail", "hdr=", hdr, "output=", output, "raw=", raw, "number=", number);
        return new HistoryOkReply();
    }

    @Override public Reply doHistorySearch(MessageHeader hdr, boolean output, boolean raw, String pattern,
                                                  boolean unique) {
        post("historySearch", "hdr=", hdr, "output=", output, "raw=", raw, "pattern=", pattern, "unique=", unique);
        return new HistoryOkReply();
    }

    @Override public void acceptUserInput(MessageHeader hdr, String value) {
        post("acceptUserInput", "hdr=", hdr, "value=", value);
        super.acceptUserInput(hdr, value);
    }

    class ExampleContext extends ExecuteContext {
        protected ExampleContext(ExecuteContext ec) {
            super(ec);
        }

        @Override public void preExecute() {
            this.writeStderr("preexecute #" + this.getExecutionCount()
                                + " id=" + this.getHeader().getMessageId() + "\n");
        }

        @Override public String execute() throws InterruptedException {
            post("executeInPool");
            switch(this.getCode().trim()) {
            case "test_hello_world":
                this.writeStdout("hello, world");
                return null;
            case "test_stdin":
                return "You said: " + this.promptStdin("Great prompt: ");
            case "test_password":
                return "You said: " + this.promptStdinPassword("Password prompt: ");
            case "test_execute_result":
                return "dummy_result";
            case "test_display_data":
                sender.postIoPubDisplayData(this.getHeader(),
                                            singletonMap("image/png", redDot),
                                            emptyMap());
                return null;
            case "test_return_image":
                this.writeResult("image/png", redDot);
                return null;
            case "test_two_results":
                this.writeResult("text/plain", "plain text\n");
                this.writeResult("text/plain", "more plain text\n");
                return null;
            case "test_code_page":
                ExecutionOkReply reply = (ExecutionOkReply) this.getExecutionReply();
                reply.addPagePayload(singletonMap("text/plain", "here is something"), 0);
                return null;
            default:
                this.writeStdout(this.getCode());
                for (int i = 1; i < 6; i++) {
                    this.writeStdout("Doing important thing " + i + "...\n");
                    Thread.sleep(100);
                }
                return "SUPER IMPORTANT RESULT.";
            }
        }
    }
}
