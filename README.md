## A kernel package for Jupyter

Hopefully this framework is flexible enough to allow arbitrary JVM languages to implement support for Jupyter.

### Where to start

The fundamental interface is [Kernel][tenet.jupyter.v5.Kernel], which has a great deal of documentation of the responses the
kernel should give.

There are a number of pitfalls, though, and [AbstractKernel][tenet.jupyter.v5.AbstractKernel] is more heavily fleshed out.

## Building from source

The package you'll actually want to deliver is the fat jar containing all dependencies in one
file; `gradle fatJar` will build `build/libs/jupyter-kernel-all-*.jar`.

### Running the example ad-hoc

Normally the console or notebook will create the connection file and call the kernel for you.

But for debugging gnarly wire issues or just doing rapid iteration on your kernel, an easy way
is to create an ad hoc connection file. Try running this:

```sh
java -cp build/libs/jupyter-kernel-all-*.jar tenet.jupyter.runner.RunnerV5 \
    --kernel-class tenet.jupyter.example.ExampleKernel
```

You can copy that into a json file, and provide the **absolute** path to 
`jupyter console --existing /abs/path/to/connection.json`.

When I'm working on the kernel, I reuse the connection file, so I'll have another tab open with:

```sh
gradle clean fatJar && \
java -cp build/libs/whatever.jar tenet.jupyter.runner.RunnerV5 \
     --kernel-class tenet.jupyter.example.ExampleKernel \
     --connection-file ~/connection.json
```

## Installation

To actually run the kernel, the user needs create a directory structure:

```
   my-kernel-dir
       kernel.json
```

The kernel spec file, `kernel.json`, should have the contents:

```json
{
  "argv": ["java", "-cp", "your classpath",
                   "tenet.jupyter.runner.RunnerV5",
                   "--kernel-class", "my.great.kernel.Kernel",
                   "--connection-file", "{connection_file}"],
  "env": {"CLASSPATH": "Can go here instead of -cp"},
  "display_name": "My Great Language",
  "language": "great"
}
```

And then the user runs:
```sh
jupyter-kernelspec install my-kernel-dir
``` 

There are more details are [in the jupyter docs][install-docs].

[tenet.jupyter.v5.Kernel]: src/main/java/tenet/jupyter/v5/Kernel.java
[tenet.jupyter.v5.AbstractKernel]: src/main/java/tenet/jupyter/v5/AbstractKernel.java
[install-docs]: https://jupyter-client.readthedocs.io/en/latest/kernels.html#kernelspecs
